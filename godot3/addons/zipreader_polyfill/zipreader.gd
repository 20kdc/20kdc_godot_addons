# zipreader_polyfill - ZIPReader polyfill for Godot 3.x
# Version 1.0 - 8th November, 2023: 20kdc: It works
# License text at bottom of file.

class_name ZIPReader
extends Reference

var _file := File.new()
var _file_list_name := PoolStringArray()
var _file_dict := {}

func open(path: String) -> int:
	close()
	var err = _file.open(path, File.READ)
	if err != OK:
		return err
	# Find EOCD
	var cd_ofs := -1
	var cd_ec := 0
	var flen := _file.get_len()
	for i in range(65536):
		var backseek := 22 + i
		if flen < backseek:
			continue
		_file.seek(flen - backseek)
		if _file.get_32() == 0x06054b50:
			# Found EOCD
			_file.get_32()
			cd_ec = _file.get_16() & 0xFFFF
			_file.get_16()
			_file.get_32()
			cd_ofs = _file.get_32() & 0xFFFFFFFF
			break
	if cd_ofs == -1:
		push_error("ZIPReader: Central Directory could not be located")
		return ERR_INVALID_DATA
	# past this point the ZIPReader is open
	_file.seek(cd_ofs)
	while cd_ec > 0:
		var cdfh := _file.get_position()
		if _file.get_32() != 0x02014b50:
			# something went wrong - abort with current successes
			break
		# Interesting side note here:
		# Godot 4.1's ZIP file reader DOES NOT ignore "dir" entries.
		# With that in mind, compatibility states neither should this one.
		_file.seek(cdfh + 28)
		var name_len := _file.get_16() & 0xFFFF
		var extra_len := _file.get_16() & 0xFFFF
		var comment_len := _file.get_16() & 0xFFFF
		_file.seek(cdfh + 42)
		var lfh := _file.get_32() & 0xFFFFFFFF
		_file.seek(cdfh + 46)
		# grab filename while we're here
		var filename := _file.get_buffer(name_len).get_string_from_utf8()
		# skip to LFH
		_file.seek(lfh)
		if _file.get_32() == 0x04034b50:
			# LFH looks right
			_file.seek(lfh + 8)
			var method := _file.get_16()
			_file.seek(lfh + 14)
			var crc32 = _file.get_32() & 0xFFFFFFFF
			var compressed_len := _file.get_32() & 0xFFFFFFFF
			var uncompressed_len := _file.get_32() & 0xFFFFFFFF
			# the contents of these are ignored
			var fn2 := _file.get_16() & 0xFFFF
			var en2 := _file.get_16() & 0xFFFF
			var pos := lfh + 30 + fn2 + en2
			# commit the data
			_file_list_name.push_back(filename)
			_file_dict[filename] = {
				"pos": pos,
				"method": method,
				"crc32": crc32,
				"compressed_len": compressed_len,
				"uncompressed_len": uncompressed_len
			}
		# done! onto the next file
		_file.seek(cdfh + 46 + name_len + extra_len + comment_len)
		cd_ec -= 1
	return OK

func close() -> int:
	_file.close()
	_file_list_name = PoolStringArray()
	_file_dict = {}
	return OK

func get_files() -> PoolStringArray:
	return _file_list_name

func read_file(path: String, case_sensitive: bool = true) -> PoolByteArray:
	var dict = _file_dict.get(path)
	if dict == null and not case_sensitive:
		for v in _file_list_name:
			if v.nocasecmp_to(path) == 0:
				dict = _file_dict[v]
				break
	if dict == null:
		# File not found!
		push_error("ZIPReader: File not found: " + path)
		return PoolByteArray()
	# consider extraction details
	var pos: int = dict["pos"]
	var method: int = dict["method"]
	var crc32: int = dict["crc32"]
	var compressed_len: int = dict["compressed_len"]
	var uncompressed_len: int = dict["uncompressed_len"]
	if method == 0:
		_file.seek(pos)
		return _file.get_buffer(uncompressed_len)
	elif method == 8:
		_file.seek(pos)
		# ok, so, this is REALLY REALLY stupid
		# but it's necessary because the Godot gods demand it
		# we don't have raw (header/footerless) DEFLATE access at all
		# but LUCKILY we can just about get a workaround due to common features
		# between GZIP and ZIP
		# stapling together a credible GZIP header/footer from what we have
		# https://datatracker.ietf.org/doc/html/rfc1952
		var cdata := StreamPeerBuffer.new()
		cdata.put_8(0x1F) # ID1
		cdata.put_8(0x8B) # ID2
		cdata.put_8(0x08) # CM
		cdata.put_8(0x00) # FLG
		cdata.put_32(0) # MTIME
		cdata.put_8(0x00) # XFL
		cdata.put_8(0x00) # OS
		cdata.put_data(_file.get_buffer(compressed_len))
		cdata.put_32(crc32)
		cdata.put_32(uncompressed_len)
		# print(Marshalls.raw_to_base64(cdata.data_array))
		var udata = cdata.data_array.decompress(uncompressed_len, File.COMPRESSION_GZIP)
		return udata
	else:
		push_error("ZIPReader: Unsupported compression method: " + path)
	return PoolByteArray()

# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>
