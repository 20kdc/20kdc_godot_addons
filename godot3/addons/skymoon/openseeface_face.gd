class_name SkymoonOpenSeeFaceFace
extends Reference

# landmarks are sort of a mess to identify because there's a LOT of them
const LANDMARK_COUNT = 68
const POINT3D_COUNT = 70
# features however are easier
const FEATURE_EYE_LEFT = 0
const FEATURE_EYE_RIGHT = 1
const FEATURE_EYEBROW_STEEP_LEFT = 2
const FEATURE_EYEBROW_UPDOWN_LEFT = 3
const FEATURE_EYEBROW_QUIRK_LEFT = 4
const FEATURE_EYEBROW_STEEP_RIGHT = 5
const FEATURE_EYEBROW_UPDOWN_RIGHT = 6
const FEATURE_EYEBROW_QUIRK_RIGHT = 7
const FEATURE_MOUTH_CORNER_VERTICAL_LEFT = 8
const FEATURE_MOUTH_CORNER_DEPTH_LEFT = 9
const FEATURE_MOUTH_CORNER_VERTICAL_RIGHT = 10
const FEATURE_MOUTH_CORNER_DEPTH_RIGHT = 11
const FEATURE_MOUTH_OPEN = 12
const FEATURE_MOUTH_WIDE = 13
const FEATURE_COUNT = 14

# header: 33 bytes
# rotational: 40 bytes
# LM arrays: 816 bytes
# 3D array: 840 bytes
# feature array: 56 bytes
const SIZE = 1785

var now: float
var face_id: int
var image_size: Vector2
# These are described from the person's perspective.
# If you close your left eye, eye_left will tend to 0.
# If you close your right eye, eye_right will tend to 0.
# This is important because of the OpenSeeFace tracker's ASCII-art eyes.
# They invert things so it's like a mirror.
# So if you close your left eye, the eye to the left on the ASCII-art closes.
# Their variables left_state and right_state are correctly named.
var eye_left: float
var eye_right: float
var success: bool
var pnp_error: float
# Note that none of this is translated between spaces.
var quaternion: Quat
var euler: Vector3
var translation: Vector3

var landmarks_c: PoolRealArray
var landmarks_pos: PoolVector2Array
var points3d: PoolVector3Array
var features: PoolRealArray

func _init():
	landmarks_c.resize(LANDMARK_COUNT)
	landmarks_pos.resize(LANDMARK_COUNT)
	points3d.resize(POINT3D_COUNT)
	features.resize(FEATURE_COUNT)

func from_buffer(b: PoolByteArray):
	var sp = StreamPeerBuffer.new()
	sp.data_array = b
	sp.big_endian = false
	# now
	now = sp.get_double()
	# face ID
	face_id = sp.get_32()
	# image size
	var e_x = sp.get_float()
	var e_y = sp.get_float()
	image_size = Vector2(e_x, e_y)
	# eyes
	eye_right = sp.get_float()
	eye_left = sp.get_float()
	# success
	success = sp.get_8() != 0
	# error
	pnp_error = sp.get_float()
	# quaternion : 4
	e_x = sp.get_float()
	e_y = sp.get_float()
	var e_z = sp.get_float()
	var e_w = sp.get_float()
	quaternion = Quat(e_x, e_y, e_z, e_w)
	# euler : 3
	e_x = sp.get_float()
	e_y = sp.get_float()
	e_z = sp.get_float()
	euler = Vector3(e_x, e_y, e_z)
	# translation : 3
	e_x = sp.get_float()
	e_y = sp.get_float()
	e_z = sp.get_float()
	translation = Vector3(e_x, e_y, e_z)
	var landmark_range = range(LANDMARK_COUNT)
	var point3d_range = range(POINT3D_COUNT)
	# C-values
	for i in landmark_range:
		landmarks_c[i] = sp.get_float()
	# X/Y-values
	for i in landmark_range:
		e_x = sp.get_float()
		e_y = sp.get_float()
		landmarks_pos[i] = Vector2(e_x, e_y)
	# XYZ positions
	for i in point3d_range:
		e_x = sp.get_float()
		e_y = sp.get_float()
		e_z = sp.get_float()
		points3d[i] = Vector3(e_x, e_y, e_z)
	# features
	for i in range(FEATURE_COUNT):
		features[i] = sp.get_float()

func converted_euler():
	var result = euler
	result += Vector3(0, -180, 90)
	result *= Vector3(1, 1, -1)
	return result
