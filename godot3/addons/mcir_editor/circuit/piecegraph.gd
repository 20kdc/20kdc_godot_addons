class_name MCIREditorPieceGraph
extends GraphEdit

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)

var names_to_nodes: Dictionary

func _ready():
	connect("node_selected", self, "_write_spn")
	connect("connection_request", self, "_connection_req")
	connect("disconnection_request", self, "_disconnection_req")
	ep.connect("circuit_changed", self, "_circuit_changed")
	ep.connect("context_changed", self, "_context_changed")
	names_to_nodes = {}
	_circuit_changed()

func get_current_piece_node() -> GraphNode:
	if names_to_nodes.has(ep.piece):
		return names_to_nodes[ep.piece]
	return null

func _write_spn(node: GraphNode):
	# forward upwards
	ep.piece = node.piece_name
	ep.exc()

func _circuit_changed():
	for v in names_to_nodes.values():
		v.get_parent().remove_child(v)
		v.queue_free()
	names_to_nodes.clear()
	var c = ep.circuit
	clear_connections()
	for name in c.pieces.keys():
		# alright, let's "render" a piece
		var gn: Control
		var pc: MCIRDataPieceBase = c.pieces[name]
		if pc is MCIRDataPiece:
			gn = preload("piecegraphnode.tscn").instance()
		elif pc is MCIRDataTie:
			gn = preload("piecegraphtie.tscn").instance()
		else:
			push_error(name + " not a piece nor a tie in pieces dictionary")
		gn.setup(pc, name)
		# finish up
		add_child(gn)
		names_to_nodes[name] = gn
		if name.begins_with("~"):
			gn.visible = false
			gn.mouse_filter = Control.MOUSE_FILTER_IGNORE
	# LEFT AND RIGHT TRACKERS
	for name in c.pieces.keys():
		var pc: MCIRDataPieceBase = c.pieces[name]
		var node1: MCIREditorPieceGraphNodeBase = names_to_nodes[name]
		for v in pc.get_connections():
			var pc2: MCIRDataPieceBase = c.pieces[v]
			var node2: MCIREditorPieceGraphNodeBase = names_to_nodes[v]
			node1.right_tracker.append([node2, node2.left_tracker])
			node2.left_tracker += 1
	# SLOT SETUP PHASE
	for nd in names_to_nodes.values():
		var node1: MCIREditorPieceGraphNodeBase = nd
		node1.setup_slots()
	# ALL CONNECTION STUFF COMMENCES
	for name in c.pieces.keys():
		var pc: MCIRDataPieceBase = c.pieces[name]
		var node1: MCIREditorPieceGraphNodeBase = names_to_nodes[name]
		var idx = 0
		for v in node1.right_tracker:
			var node2: MCIREditorPieceGraphNodeBase = v[0]
			var sn1 = node1.get_right_slot_index(idx)
			var sn2 = node2.get_left_slot_index(v[1])
			connect_node(node1.name, sn1, node2.name, sn2)
			idx += 1
	_context_changed()

func _context_changed():
	# select if not already
	var pn = get_current_piece_node()
	if pn != null:
		if not pn.selected:
			set_selected(pn)

func _connection_req(f_i: String, fs: int, t_i: String, ts: int):
	var f: String = get_node(f_i).piece_name
	var t: String = get_node(t_i).piece_name
	print("connection request, " + f + " to " + t)
	if not ep.circuit.pieces.has(f):
		return
	if not ep.circuit.pieces.has(t):
		return
	var p: MCIRDataPieceBase = ep.circuit.pieces[f]
	p.add_connection(t)
	ep.ecc()

func _disconnection_req(f_i: String, fs: int, t_i: String, ts: int):
	var f: String = get_node(f_i).piece_name
	var t: String = get_node(t_i).piece_name
	print("disconnection request, " + f + " to " + t)
	if not ep.circuit.pieces.has(f):
		return
	var p: MCIRDataPieceBase = ep.circuit.pieces[f]
	p.del_connection(t)
	ep.ecc()
