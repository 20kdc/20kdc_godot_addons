extends Control

signal piano_key(k, p)

var piano_base_idx: int = 17
var piano_physical_scancodes: Array = [
	KEY_BACKSLASH,
	KEY_Z,
	KEY_X,
	KEY_C,
	KEY_V,
	KEY_B,
	KEY_N,
	KEY_M,
	KEY_COMMA,
	KEY_PERIOD,
	KEY_SLASH,
	KEY_A,
	KEY_S,
	KEY_D,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_SEMICOLON,
	KEY_APOSTROPHE,
	KEY_Q,
	KEY_W,
	KEY_E,
	KEY_R,
	KEY_T,
	KEY_Y,
	KEY_U,
	KEY_I,
	KEY_O,
	KEY_P,
	# major difference here between phys. and non-phys.
	KEY_BRACKETLEFT,
	KEY_BRACKETRIGHT,
	KEY_NUMBERSIGN
]

func _gui_input(event):
	if event is InputEventKey:
		var key: InputEventKey = event
		print(key.scancode)
		# TODO: this was physical_scancode and that worked, but then it didn't.
		# suspect this is waiting on 3.4
		var sc = key.scancode
		var idx = piano_physical_scancodes.find(sc)
		if idx != -1:
			print("Piano: IDX " + str(idx) + " P: " + str(key.pressed))
			emit_signal("piano_key", idx - piano_base_idx, key.pressed)
