class_name MCIREditorPlaybackExecutive
extends Node

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)

var running_patterns = []
var _last_process_us: int = OS.get_ticks_usec()

signal playing_changed()
signal started_pattern_playback(pt, st, note, px)

func _on_stop_pressed():
	for v in running_patterns:
		if is_instance_valid(v):
			v.queue_free()
	running_patterns.clear()
	emit_signal("playing_changed")

func _process(delta):
	var now = OS.get_ticks_usec()
	var advance = now - _last_process_us
	_last_process_us = now
	for v in running_patterns:
		if is_instance_valid(v):
			if v is MCIRExecAdvancable:
				v.mcir_advance_us(advance)

func inject(n: Node):
	add_child(n)
	running_patterns.append(n)

func is_playing_piece(s: String) -> bool:
	for v in running_patterns:
		if is_instance_valid(v):
			if v is MCIREditorPieceFollowController:
				if v.piece == s:
					if not v.is_queued_for_deletion():
						return true
	return false

func play_pattern(s: String, pos: float, note: float):
	if ep.circuit.patterns.has(s):
		return play_pattern_obj(ep.circuit.patterns[s], pos, note)
	return null

func play_pattern_obj(s: MCIRDataPatternBase, pos: float, note: float):
	var env = MCIRExecEnvironmentalData.new()
	env.circuit = ep.circuit
	env.start_pos = pos
	env.ival.note = note
	var pat: MCIRExecPatternBase = MCIRExecPatterns.run(s, self, env)
	running_patterns.append(pat)
	emit_signal("playing_changed")
	emit_signal("started_pattern_playback", s, pos, note, pat)
	return pat

func play_piece(s: String, pos: float, follow: bool):
	for i in range(32):
		if not ep.circuit.pieces.has(s):
			return
		var piece_base: MCIRDataPieceBase = ep.circuit.pieces[s]
		if piece_base is MCIRDataPiece:
			var piece: MCIRDataPiece = piece_base
			var pfc = MCIREditorPieceFollowController.new()
			for v in piece.patterns:
				var res = play_pattern(v, pos, 0.0)
				pfc.held_patterns.append(res)
			# determine next piece
			pfc.time_left = piece.beat_time_usec(piece.beats)
			pfc.piece = s
			pfc.follow = follow
			inject(pfc)
			emit_signal("playing_changed")
			return
		elif piece_base is MCIRDataTie:
			var tie: MCIRDataTie = piece_base
			s = tie.target
		else:
			push_error("Encountered unknown piece type")
	push_warning("Ran into long loop during piece navigation")

func play_reel_note(r: MCIRDataPatternReel, nt: MCIRDataPatternReelNote):
	var ntx = MCIRExecPatternReelNoteQADSelfTimed.new()
	var env = MCIRExecEnvironmentalData.new()
	env.circuit = ep.circuit
	# default aftertouch settings are fine, so don't apply_aftertouch
	ntx.setup(nt, env)
	ntx.st_beat_time = MCIRMaths.beat_time(r.bpm, 1.0)
	inject(ntx)

func piecefollowcontroller_done_hook():
	emit_signal("playing_changed")

func _on_playpiece_pressed():
	_on_stop_pressed()
	play_piece(ep.piece, 0.0, true)

static func of(n: Node) -> Node:
	return MCIREditorEntrypoint.of(n).get_node("playback_executive")
