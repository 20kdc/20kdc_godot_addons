class_name MCIREditorSSE
extends VBoxContainer

onready var le: LineEdit = $hbc/val
var value: PoolStringArray

signal user_edited()
signal user_selected(s)

func suggest(s: String):
	le.text = s

func set_value(v: PoolStringArray):
	if value == v:
		return
	value = v
	$ItemList2.clear()
	for v in value:
		$ItemList2.add_item(v)

func _on_adp_pressed():
	for v in value:
		if v == le.text:
			MCIREditorEntrypoint.of(self).msgbox("already there")
			return
	# copies etc.
	var v2 = value
	v2.append(le.text)
	set_value(v2)
	emit_signal("user_edited")

func _on_rmp_pressed():
	var idx = 0
	for v in value:
		if v == le.text:
			# this copies, which is important
			# editing value in-place won't work
			var v2 = value
			v2.remove(idx)
			set_value(v2)
			emit_signal("user_edited")
			return
		idx += 1
	MCIREditorEntrypoint.of(self).msgbox("not found")

func _on_ItemList2_item_activated(index):
	# actually selected for convenience
	suggest(value[index])
	emit_signal("user_selected", value[index])
