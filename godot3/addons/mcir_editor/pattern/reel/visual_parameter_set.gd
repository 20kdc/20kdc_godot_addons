class_name MCIREditorPatternReelVPS
extends Control

onready var re: MCIREditorReelPatEd = MCIREditorReelPatEd.of(self)
onready var pe: MCIREditorPlaybackExecutive = MCIREditorPlaybackExecutive.of(self)
onready var backboard: Control = $backboard

const HEADERSPACE: Vector2 = Vector2(0, 24)
const BASENOTE: int = 24
const NOTES: int = 48
const C_S: Vector2 = Vector2(32, 16)

var delthese = []
var notenodes = {}
var _selected_note_node: MCIREditorReelNoteControl = null

func _ready():
	re.reel.connect("changed", self, "_changed")
	re.connect("note_changed", self, "_note_changed")
	_changed()

func _changed():
	for v in delthese:
		v.queue_free()
	delthese.clear()
	notenodes.clear()
	backboard.rect_position = HEADERSPACE
	backboard.rect_size = Vector2(re.reel.editor_beats, NOTES) * C_S
	backboard.update()
	rect_min_size = backboard.rect_position + backboard.rect_size
	for i in range(floor(re.reel.editor_beats)):
		var b = Button.new()
		b.text = "v"
		b.rect_position = Vector2(i, 0) * C_S
		b.rect_size = (Vector2(1, 0) * C_S) + HEADERSPACE
		b.connect("pressed", self, "_play_from", [i])
		add_child(b)
		delthese.append(b)
	for v in re.reel.notes:
		var n: MCIRDataPatternReelNote = v
		var b: MCIREditorReelNoteControl = preload("reel_note.tscn").instance()
		b.set_pattern(n.pattern)
		b.set_selected(false)
		b.rect_position = (Vector2(n.start, map_note_br_fwd(n.note)) * C_S) + HEADERSPACE
		b.rect_size = Vector2(n.length, 1) * C_S
		b.connect("selected", self, "_selected_note", [n])
		b.connect("resize_confirm", self, "_resize_note", [n])
		b.connect("move_confirm", self, "_move_note", [n])
		# change feedback mechanism
		n.connect("changed", b, "emit_signal", ["feedback_change"])
		b.connect("feedback_change", self, "_changed")
		add_child(b)
		delthese.append(b)
		notenodes[v] = b
	_note_changed()

func _note_changed():
	if is_instance_valid(_selected_note_node):
		_selected_note_node.set_selected(false)
		_selected_note_node = null
	if not notenodes.has(re.note):
		return
	var nn: MCIREditorReelNoteControl = notenodes[re.note]
	nn.set_selected(true)
	_selected_note_node = nn

func _selected_note(n: MCIRDataPatternReelNote):
	pe.play_reel_note(re.reel, n)
	re.set_note(n)

func _resize_note(w: float, n: MCIRDataPatternReelNote):
	n.length = max(1.0, round(w / C_S.x))
	n.emit_changed()

func _move_note(p: Vector2, n: MCIRDataPatternReelNote):
	p -= HEADERSPACE
	re.reel.rm_note(n)
	n.start = max(0.0, round(p.x / C_S.x))
	n.note = map_note_br_bck(round(p.y / C_S.y))
	n.emit_changed()
	re.reel.add_note(n)

func _play_from(i):
	var time: float = MCIRMaths.beat_time(re.reel.bpm, i) - 0.001
	pe.play_pattern_obj(re.reel, time, 0.0)

# add basenote & reverse
static func map_note_br_fwd(y: float) -> float:
	return BASENOTE - y

# remove basenote & reverse
static func map_note_br_bck(y: float) -> float:
	return BASENOTE - y

func create_note_at_baseboard(p: Vector2):
	var n = MCIRDataPatternReelNote.new()
	# no need to remove headerspace for a baseboard location
	n.start = max(0.0, floor(p.x / C_S.x))
	n.note = map_note_br_bck(floor(p.y / C_S.y))
	n.pattern = re.cached_default_pattern
	re.reel.add_note(n)
	pe.play_reel_note(re.reel, n)
