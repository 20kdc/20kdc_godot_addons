extends Control

onready var re: MCIREditorReelPatEd = MCIREditorReelPatEd.of(self)
onready var le: LineEdit = $le
onready var ple: MCIREditorPatternListEmbedded = $ple

func _ready():
	ple.connect("pattern_selected", self, "_pattern_selected")
	re.connect("note_changed", self, "_note_changed")

func _note_changed():
	if re.note != null:
		le.text = re.note.pattern
		re.cached_default_pattern = le.text
		ple.select_item(re.note.pattern)

func _pattern_selected(p):
	re.cached_default_pattern = p
	if re.note != null:
		re.note.pattern = p
		re.note.emit_changed()

func _on_le_text_entered(new_text):
	_pattern_selected(new_text)
