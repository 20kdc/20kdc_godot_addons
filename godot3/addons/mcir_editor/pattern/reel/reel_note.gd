class_name MCIREditorReelNoteControl
extends Panel

signal selected()
signal resize_confirm(w)
signal move_confirm(p)
signal feedback_change()

func set_selected(sel: bool):
	var sb: StyleBox = preload("reel_note_unselected.tres")
	if sel:
		sb = preload("reel_note_selected.tres")
	self.add_stylebox_override("panel", sb)

func set_pattern(text: String):
	$Label.text = text
