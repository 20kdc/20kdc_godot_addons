extends Control

export var mover: bool = false

var _busy = false
var _b_original: Vector2
var _b_pos: Vector2

func _gui_input(event):
	if event is InputEventMouseButton:
		var mb: InputEventMouseButton = event
		if mb.button_index == BUTTON_LEFT:
			_busy = mb.pressed
			if _busy:
				if not mover:
					_b_original = get_parent().rect_size
				else:
					_b_original = get_parent().rect_position
				_b_pos = mb.global_position
			else:
				if not mover:
					get_parent().emit_signal("resize_confirm", get_parent().rect_size.x)
				else:
					get_parent().emit_signal("move_confirm", get_parent().rect_position)
			accept_event()
		if mb.pressed:
			get_parent().emit_signal("selected")
	elif event is InputEventMouseMotion:
		var mb: InputEventMouseMotion = event
		if _busy:
			var delta: Vector2 = mb.global_position - _b_pos
			if not mover:
				get_parent().rect_size = Vector2(_b_original.x + delta.x, _b_original.y)
			else:
				get_parent().rect_position = _b_original + delta
