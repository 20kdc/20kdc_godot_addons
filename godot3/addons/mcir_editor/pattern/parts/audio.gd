extends MCIREditorPatEdBase

onready var ep: MCIREditorEntrypoint = MCIREditorEntrypoint.of(self)
onready var loadfile: Button = $hbc/loadfile
onready var cont: CheckBox = $hbc/cont
onready var stats: Label = $stats

var aud: MCIRDataPatternAudio

func mcir_pated_setup(pat: MCIRDataPatternAudio):
	aud = pat
	aud.connect("changed", self, "_changed")

func _ready():
	_changed()

func _changed():
	if aud.stream != null:
		loadfile.text = str(aud.stream.resource_path)
	else:
		loadfile.text = "Load file..."
	cont.pressed = aud.release_continue
	var st: String = ""
	st += "Statistics\n"
	if aud.stream != null:
		st += "Length: " + str(aud.stream.get_length()) + "\n"
		if aud.stream is AudioStreamSample:
			var ax: AudioStreamSample = aud.stream
			st += "RAW\n"
			st += "LoopMode: " + str(ax.loop_mode) + " | B " + str(ax.loop_begin) + " | E " + str(ax.loop_end) + "\n"
			st += "Bytes: " + str(len(ax.data)) + "\n"
		elif aud.stream is AudioStreamMP3:
			var ax: AudioStreamMP3 = aud.stream
			st += "MP3\n"
			st += "Loops: " + str(ax.loop) + " | O " + str(ax.loop_offset) + "\n"
			st += "Bytes: " + str(len(ax.data)) + "\n"
		elif aud.stream is AudioStreamOGGVorbis:
			var ax: AudioStreamOGGVorbis = aud.stream
			st += "Vorbis\n"
			st += "Loops: " + str(ax.loop) + " | O " + str(ax.loop_offset) + "\n"
			st += "Bytes: " + str(len(ax.data)) + "\n"
		elif aud.stream is AudioStreamGenerator:
			st += "Procedural\n"
		elif aud.stream is AudioStreamRandomPitch:
			st += "Random-Pitch (Wraps another audio stream!)\n"
		else:
			st += "Unknown Codec\n"
	stats.text = st

func _on_loadfile_pressed():
	var fd = FileDialog.new()
	fd.mode = FileDialog.MODE_OPEN_FILE
	ep.fixup_file_dialog(fd, self, "_loadfile")

func _loadfile(s: String):
	var ax = load(s)
	if ax is AudioStream:
		aud.stream = ax
		aud.emit_changed()
	else:
		ep.msgbox("That's no audio stream!")

func _on_clear_pressed():
	aud.stream = null
	aud.emit_changed()

func _on_cont_toggled(button_pressed):
	aud.release_continue = button_pressed
	aud.emit_changed()
