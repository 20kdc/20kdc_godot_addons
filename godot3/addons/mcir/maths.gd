class_name MCIRMaths
extends Object

static func beat_time(bpm: float, beat: float) -> float:
	var time_per_beat = 60.0 / bpm
	return time_per_beat * beat

static func beat_time_us(bpm: float, beat: float) -> int:
	return int(beat_time(bpm, beat * 1000000))

static func time_beat(bpm: float, time: float) -> float:
	var time_per_beat = 60.0 / bpm
	return time / time_per_beat
