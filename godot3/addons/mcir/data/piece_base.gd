class_name MCIRDataPieceBase
extends Resource

# name is implicit
export var editor_pos: Vector2 = Vector2.ZERO

func get_connections() -> PoolStringArray:
	return PoolStringArray()

func add_connection(a: String):
	push_error("UNIMPLEMENTED: add_connection")

func del_connection(a: String):
	push_error("UNIMPLEMENTED: del_connection")

func rename_connections(a: String, b: String):
	push_error("UNIMPLEMENTED: rename_connections")

func has_connection(a: String):
	for v in get_connections():
		if v == a:
			return true
	return false

func rename_pattern_refs(a: String, b: String) -> int:
	return 0
