class_name MCIRDataTie
extends MCIRDataPieceBase

# Target piece that this tie connects to.
export var target: String = ""

func get_connections() -> PoolStringArray:
	if target == "":
		return PoolStringArray()
	return PoolStringArray([target])

func add_connection(a: String):
	target = a

func del_connection(a: String):
	target = ""

func rename_connections(a: String, b: String):
	if target == a:
		target = b
