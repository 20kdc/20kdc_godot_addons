class_name MCIRDataPatternAudio
extends MCIRDataPatternBase

export var stream: AudioStream = null
export var release_continue: bool = false

func mcir_pattern_id() -> String:
	return "audio"
