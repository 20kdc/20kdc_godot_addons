class_name MCIRDataPatternBase
extends Resource

export var ignore_note: bool = false

# BE WARNED!
# Patterns do not use the global update callback!
# They instead use their internal "changed" signals!
# This prevents pattern editors rebuilding the internal editors and causing no end of trouble.

func mcir_pattern_id() -> String:
	return "base"

func rename_pattern_refs(from: String, to: String) -> int:
	return 0

func dup_pattern():
	return duplicate()
