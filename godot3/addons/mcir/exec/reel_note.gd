class_name MCIRExecPatternReelNote
extends MCIRExecAdvancable

# These default values are important.
# They control the settings that might be used if a note were to be played
#  outside of a pattern.
# Luckily the base constructor is enough for this.
var at_base: MCIRExecAftertouchState
var at_current: MCIRExecAftertouchState

var note_rpn: float
var released: bool = false

var note: MCIRDataPatternReelNote = null
var notei: MCIRExecPatternBase = null

# current position in note's relative timeline, in beats
var relative_beat: float = 0

# metadata...
# absolute tracking.
# this is used when a sustain loop has a note 'around' it.
var mt_tracking_abs: bool = false
# start microseconds, used for absolute tracking
var mt_s_us: int = 0

func _init():
	at_base = MCIRExecAftertouchState.new()
	at_current = MCIRExecAftertouchState.new()

func apply_aftertouch(from: MCIRExecAftertouchState):
	at_base.copy(from)
	if note != null:
		_recalculate(true)

func setup(nt: MCIRDataPatternReelNote, base_env: MCIRExecEnvironmentalData):
	note = nt
	var circuit: MCIRDataCircuit = base_env.circuit
	if not circuit.patterns.has(note.pattern):
		return
	# setup env
	var my_env = MCIRExecEnvironmentalData.new()
	# Note that aftertouch is NOT copied from base_env.
	# Aftertouch is applied through apply_aftertouch.
	# Then *that* aftertouch is applied in _recalculate as usual,
	#  and is forwarded through at_current.
	if not my_env.copy_global_add(base_env, note.pattern):
		return
	note_rpn = note.note
	_recalculate(false)
	my_env.ival.copy(at_current)
	# ready to start
	notei = MCIRExecPatternsDC.run(circuit.patterns[note.pattern], self, my_env)

func _recalculate(apply: bool):
	at_current.copy(at_base)
	# modulate
	at_current.note += note_rpn
	at_current.note += note.lg_pitch.sample(relative_beat, 0.0)
	at_current.vol_mul *= note.lg_volume.sample(relative_beat, 1.0)
	if apply:
		if is_instance_valid(notei):
			notei.mcir_exec_pattern_aftertouch(at_current)

func release():
	if not released:
		if is_instance_valid(notei):
			notei.mcir_exec_pattern_release()
		released = true

# *This function updates the timeline.*
func set_relative_beat(v: float):
	relative_beat = v
	if v >= note.length:
		release()
	_recalculate(true)

func mcir_advance_us(us: int):
	if not is_instance_valid(notei):
		queue_free()
	else:
		notei.mcir_advance_us(us)
