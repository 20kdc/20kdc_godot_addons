class_name MCIRExecAdvancable
extends Node

# This is used for exact time tracking.
# Note that advance tracking is handled manually because of some complex cases
#  involving sustain/etc.
# Specifically, if a note start occurs too early, a situation can occur where
#  the note has begin another sustain loop just before it gets terminated.
# Tracking things in microseconds prevents this kind of problem, as in practice,
#  notes are moved to being slightly too *late*.
# Choices here are to either cut off the note's start,
#  or to just let it go.

func mcir_advance_us(us: int):
	pass
