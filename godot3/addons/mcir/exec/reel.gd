extends MCIRExecPatternBase

# BEFORE YOU CONTINUE...
# The boundary cases on this get complicated, so:
# 1. All measurements are in microseconds.
# 2. Floating point calculations are assumed to be repeatable.
#    Therefore, for a given beat, and a given BPM,
#    there is one integer amount of microseconds.
# 3. Microsecond 0 is executed and corresponds to actions at the start of beat 0.
# 4. Starts on the sustain end time are never processed.
# 5. Starts on the sustain start time are processed.
# 6. A note start is executed on the time of the beat of the note start.
#    Note that the 'no-executing sustain end' rule prevents this from causing issues.
# 7. Note ends and note curves are tracked in the note in floating-point beats.
# 8. There are two modes of note execution.
#    1. Relative-time
#    2. Absolute-time
#    Absolute-time MUST be used when a note enters and leaves the sustain loop.
#    Otherwise relative-time is always better.
# 9. Execution occurs in regions with a start and end.
#    The start is the current value of the timeline.
#    The end is the final value of the timeline.
#    The current value of the timeline has already been executed.
#    Therefore, to execute starting at a specific point,
#     one must go to a microsecond before and run one microsecond.

var notes: Array
var note_t_start: Array # for bsearch
var note_t_end = PoolIntArray()
var base_env: MCIRExecEnvironmentalData
# timing:
# timeline position - update using _forward_timeline to execute notes correctly.
# note that the current value of this is assumed to have already been processed.
var timeline_us: int = -1
# timeline end - used to determine when the reel can be safely turned off
var timeline_end_us: int = 0
# sustain controls
# note: releasing the pattern is handled by disabling sustain.
var sustain_enable: bool = false
var sustain_start_us: int = 0
# Sustain end.
var sustain_end_us: int = 0
# Microseconds per beat for conversions
var beat_us: float = 0

func mcir_apply_pattern_aftertouch():
	for v in get_children():
		_forward_aftertouch(v)

func _forward_aftertouch(v: MCIRExecPatternReelNote):
	v.apply_aftertouch(stored)

func mcir_exec_pattern_setup(data: MCIRDataPatternReel, env: MCIRExecEnvironmentalData):
	notes = []
	base_env = env
	note_t_start = []
	timeline_us = int(env.start_pos * 1000000) - 1
	beat_us = MCIRMaths.beat_time(data.bpm, 1000000)
	sustain_start_us = MCIRMaths.beat_time_us(data.bpm, data.sustain_start)
	sustain_end_us = MCIRMaths.beat_time_us(data.bpm, data.sustain_end)
	sustain_enable = sustain_start_us != sustain_end_us
	for v in data.notes:
		var vn: MCIRDataPatternReelNote = v
		var ts_us = MCIRMaths.beat_time_us(data.bpm, vn.start)
		var te_us = MCIRMaths.beat_time_us(data.bpm, vn.start + vn.length)
		note_t_start.append(ts_us)
		note_t_end.append(te_us)
		notes.append(v)
		if te_us > timeline_end_us:
			timeline_end_us = te_us

func mcir_exec_pattern_release():
	sustain_enable = false

func mcir_exec_pattern_time() -> float:
	return timeline_us / 1000000.0

func _ready():
	# kickstart processing
	_forward_timeline(1)

func mcir_advance_us(us: int):
	_forward_timeline(us)
	# need to work out when to stop
	if timeline_us >= timeline_end_us:
		if get_child_count() == 0:
			queue_free()

func _forward_timeline(by: int):
	var new_us: int = timeline_us + by
	if sustain_enable and new_us >= sustain_end_us:
		var before = sustain_end_us - timeline_us
		var after = by - before
		# note the -1, don't execute the sustain end itself
		_forward_timeline(before - 1)
		# go back a microsecond before sustain start, then execute the stuff after,
		#  + 1 us for the missed microsecond previously
		timeline_us = sustain_start_us - 1
		_forward_timeline(after + 1)
		return
	# -- this should not fail --
	if by < 0:
		print("attempted to go back in time using _forward_timeline. do not do this.")
		return
	# -- regular "A to B" transition. 'by' is an accurate delta. --
	# update notes...
	# note: this must occur before starting notes,
	#  because the -1 to 0 transition uses a 1us 'kickstart'
	for v in get_children():
		var vx: MCIRExecPatternReelNote = v
		if vx.mt_tracking_abs:
			# absolute tracking
			vx.set_relative_beat((timeline_us - vx.mt_s_us) / beat_us)
		else:
			# relative tracking
			vx.set_relative_beat(vx.relative_beat + (by / beat_us))
		vx.mcir_advance_us(by)
	# start notes...
	var first_note_index = note_t_start.bsearch(timeline_us, true)
	var last1_note_index = note_t_start.bsearch(new_us, false)
	for i in range(first_note_index, last1_note_index):
		var st = note_t_start[i]
		if st > timeline_us and st <= new_us:
			# print("note", st, ",", timeline_us, ",", new_us)
			# ooo, a note to start!
			var note = MCIRExecPatternReelNote.new()
			note.apply_aftertouch(stored)
			note.setup(notes[i], base_env)
			# determine timing type:
			# absolute if start & end are outside sustain boundaries,
			#  and sustain is enabled.
			# note that the end of the note must be *after* the sustain end,
			#  since otherwise end actions can get eaten.
			if sustain_enable:
				note.mt_tracking_abs = (st < sustain_start_us) and (note_t_end[i] > sustain_end_us)
			add_child(note)
	timeline_us = new_us
