class_name MCIRExecAftertouchState
# Not a Resource, you aren't supposed to be saving these.
extends Reference

# Initial values (applied automatically in pattern_base)
export var vol_mul = 1.0
export var note = 0.0

func copy(src):
	vol_mul = src.vol_mul
	note = src.note
