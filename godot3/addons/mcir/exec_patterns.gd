class_name MCIRExecPatterns
extends Object

static func _preload_all_possible_exec_classes():
	# preloads all possible exec classes
	# this helps reduce potential unused class elimination problems
	preload("exec/audio.gd")
	preload("exec/base.gd")
	preload("exec/reel.gd")

static func run(pattern: MCIRDataPatternBase, parent: Node, env: MCIRExecEnvironmentalData) -> MCIRExecPatternBase:
	var ld = load("res://addons/mcir/exec/" + pattern.mcir_pattern_id() + ".gd")
	var mv: MCIRExecPatternBase = ld.new()
	parent.add_child(mv)
	mv.base_ignore_note = pattern.ignore_note
	mv.mcir_exec_pattern_aftertouch(env.ival)
	mv.mcir_exec_pattern_setup(pattern, env)
	return mv
