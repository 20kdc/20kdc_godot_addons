tool
extends Node
class_name ShotBakerySetup

const BAKE_HANDLER_MODE_SETUP = 0
const BAKE_HANDLER_MODE_FINISH = 1

# By using a ShotBakerySetupCubemapRecord,
#  you can collate all of the cubemaps inside this Setup.
# Please note that any interior Setup will isolate itself from this feature.
export var cubemap_record_path: String = ""

var setup_cubemap_record: ShotBakerySetupCubemapRecord = null

func _bake_subs(obj: Node, mode: int):
	for v in obj.get_children():
		# keep in mind that inner setups take over
		# this prevents them from 'stealing' record data
		if v.has_method("shotbakery_bake_handler"):
			v.shotbakery_bake_handler(mode, self)
		else:
			_bake_subs(v, mode)

func shotbakery_bake_handler(mode: int, setup: ShotBakerySetup):
	if mode == BAKE_HANDLER_MODE_SETUP:
		setup_cubemap_record = null
		# ignore setup
		if cubemap_record_path != "":
			setup_cubemap_record = ShotBakerySetupCubemapRecord.new()
	_bake_subs(self, mode)
	if mode == BAKE_HANDLER_MODE_FINISH:
		if setup_cubemap_record != null:
			ResourceSaver.save(cubemap_record_path, setup_cubemap_record)
