tool
extends EditorSpatialGizmoPlugin
class_name ShotBakeryCubemapGizmoPlugin

func _init():
	create_material("lines", Color.red)

func get_priority():
	return 0

func has_gizmo(spatial):
	if spatial is ShotBakeryCubemap:
		return true

func get_name():
	return "ShotBakeryCubemap"

func redraw(gizmo):
	gizmo.clear()
	var plugin: EditorPlugin = get_meta("editor_plugin")
	var n: ShotBakeryCubemap = gizmo.get_spatial_node()
	# don't draw anything for an unselected cubemap
	if plugin.get_editor_interface().get_selection().get_selected_nodes().count(n) == 0:
		return
	var zf = n.z_far
	var zn = n.z_near
	var sz = n.frustrum_size
	var lines = []
	_push_back_frustrum(lines, sz, zn, zf, Vector3( 1,  0,  0), Vector3(0,1,0), Vector3(0,0,1))
	_push_back_frustrum(lines, sz, zn, zf, Vector3(-1,  0,  0), Vector3(0,1,0), Vector3(0,0,1))
	_push_back_frustrum(lines, sz, zn, zf, Vector3( 0,  1,  0), Vector3(1,0,0), Vector3(0,0,1))
	_push_back_frustrum(lines, sz, zn, zf, Vector3( 0, -1,  0), Vector3(1,0,0), Vector3(0,0,1))
	_push_back_frustrum(lines, sz, zn, zf, Vector3( 0,  0,  1), Vector3(0,1,0), Vector3(1,0,0))
	_push_back_frustrum(lines, sz, zn, zf, Vector3( 0,  0, -1), Vector3(0,1,0), Vector3(1,0,0))
	gizmo.add_lines(PoolVector3Array(lines), get_material("lines", gizmo))

func _push_back_frustrum(lines: Array, sz, zn, zf, tp, tx, ty):
	# FIRST AND FOREMOST:
	# At zn=1,sz=2, everything just works.
	# Why is this?
	# Well, sz is the frustrum size (along square edges) at the near point.
	# Therefore so long as zn = sz / 2 things are fine.
	# Then the far size is extrapolated from this.
	# near/far bases
	var tn = tp * zn
	var tf = tp * zf
	# near/far sizes (expressed in radius style)
	var sn = sz / 2
	var sf = (sn / zn) * zf
	# point basises
	var p0 = -(tx + ty)
	var p1 = tx - ty
	var p2 = tx + ty
	var p3 = ty - tx
	# points
	var ns0 = tn + (sn * p0)
	var ns1 = tn + (sn * p1)
	var ns2 = tn + (sn * p2)
	var ns3 = tn + (sn * p3)
	var fs0 = tf + (sf * p0)
	var fs1 = tf + (sf * p1)
	var fs2 = tf + (sf * p2)
	var fs3 = tf + (sf * p3)
	# out lines
	lines.push_back(ns0)
	lines.push_back(fs0)
	lines.push_back(ns1)
	lines.push_back(fs1)
	lines.push_back(ns2)
	lines.push_back(fs2)
	lines.push_back(ns3)
	lines.push_back(fs3)
	# square n
	lines.push_back(ns0)
	lines.push_back(ns1)
	lines.push_back(ns1)
	lines.push_back(ns2)
	lines.push_back(ns2)
	lines.push_back(ns3)
	lines.push_back(ns3)
	lines.push_back(ns0)
	# square f
	lines.push_back(fs0)
	lines.push_back(fs1)
	lines.push_back(fs1)
	lines.push_back(fs2)
	lines.push_back(fs2)
	lines.push_back(fs3)
	lines.push_back(fs3)
	lines.push_back(fs0)
