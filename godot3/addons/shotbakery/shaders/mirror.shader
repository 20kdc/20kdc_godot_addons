shader_type spatial;
render_mode depth_draw_opaque,cull_back,unshaded,world_vertex_coords;

uniform samplerCube cube;

varying vec3 normal;

void vertex() {
	normal = NORMAL;
}

void fragment() {
	vec3 normal2 = normalize(reflect(VIEW, NORMAL));
	normal2 = -(vec4(normal2, 1.0) * INV_CAMERA_MATRIX).xyz;
	ALBEDO = texture(cube, normal2).rgb;
}
