tool
extends Resource
class_name ShotBakerySetupCubemapRecord

export var positions: PoolVector3Array
export var names: PoolStringArray
export var notes: PoolStringArray
export var cubemaps: Array
export var cubemaps_diffuse: Array

func _init():
	cubemaps = []
	cubemaps_diffuse = []

func clear():
	positions.resize(0)
	notes.resize(0)
	cubemaps.clear()
	cubemaps_diffuse.clear()

func add_cubemap(where: Vector3, what: CubeMap, what2: CubeMap, name: String, note: String):
	positions.append(where)
	names.append(name)
	notes.append(note)
	cubemaps.append(what)
	cubemaps_diffuse.append(what)
