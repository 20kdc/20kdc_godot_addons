# Encapsulation for token data in a format that doesn't require scripts to generate.
class_name MapIOMapTokenStream

var texts: PoolStringArray = PoolStringArray()
var flags: PoolIntArray = PoolIntArray()
var ptr: int = 0

func is_symbol(base: String) -> bool:
	return flags[ptr] and (texts[ptr] == base)

func remaining() -> bool:
	# print(ptr)
	return ptr < len(texts)

func add_token(text: String, symbol: bool):
	texts.push_back(text)
	if symbol:
		flags.push_back(1)
	else:
		flags.push_back(0)

