class_name MapIOTokenizerHub
extends Reference

# See methods below
enum TokenizerMethod {
	AUTOMATIC,
	GDS_RI,
	GDS_LBW_RI,
	GDS_RI_QBSP,
	GDS_LBW_RI_QBSP
}

const TOKENIZER_METHOD_HINT = "Automatic (LBW_RI),GDScript Reference Implementation (slow but compatible),GDScript Line-Based (fast but potential issues with embedded newlines),GDScript Reference Implementation QBSP Mode (no backslash string escapes),GDScript Line-Based QBSP Mode (same basic idea as before)"

static func tokenize(f: File, method: int) -> MapIOMapTokenStream:
	if method == TokenizerMethod.GDS_RI:
		return tokenize_gds_ri(f, true)
	if method == TokenizerMethod.GDS_RI_QBSP:
		return tokenize_gds_ri(f, false)
	if method == TokenizerMethod.GDS_LBW_RI_QBSP:
		return tokenize_gds_lbw_ri(f, false)
	# Default: GDS_LBW_RI
	return tokenize_gds_lbw_ri(f, true)

# Tokenize an entire file using the reference tokenizer.
# This is the most accurate GDScript-only method of tokenization available.
static func tokenize_gds_ri(f: File, permit_escapes: bool) -> MapIOMapTokenStream:
	var ts = MapIOMapTokenStream.new()
	var tkn = MapIOMapTokenizer.new()
	tkn.map = f.get_as_text()
	tkn.permit_escapes = permit_escapes
	while tkn.read_token():
		ts.add_token(tkn.current_token_text, tkn.current_token_symbol)
	return ts

# Wraps the reference tokenizer to be line-based.
# This is not necessarily accurate. It should work for any expected input.
# However, the speed boosts are quite real.
static func tokenize_gds_lbw_ri(f: File, permit_escapes: bool) -> MapIOMapTokenStream:
	var ts = MapIOMapTokenStream.new()
	var tkn = MapIOMapTokenizer.new()
	tkn.permit_escapes = permit_escapes
	while not f.eof_reached():
		var ln = f.get_line()
		tkn.map = ln
		tkn.map_ptr = 0
		while tkn.read_token():
			ts.add_token(tkn.current_token_text, tkn.current_token_symbol)
	return ts
