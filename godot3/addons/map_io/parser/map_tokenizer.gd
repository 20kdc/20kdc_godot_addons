# Reference tokenizer.
# Optimized but tries to avoid cutting too many corners.
class_name MapIOMapTokenizer
extends Reference

# Input buffer
var map: String = ""
var map_ptr: int = 0

# Setting to permit escapes (QBSP doesn't actually support this)
var permit_escapes: bool = true

var current_token_symbol: bool = false
var current_token_text: String = ""

# Returns true on EOF
func _seek_past_whitespace():
	# "GC" the string per-token
	map = map.substr(map_ptr)
	map_ptr = 0
	while map_ptr < len(map):
		if map.ord_at(map_ptr) > 32:
			# Comments
			if map.substr(map_ptr, 2) == "//":
				var ptr = map.find("\n", map_ptr)
				if ptr == -1:
					map_ptr = len(map)
					return true
				map_ptr = ptr + 1
			else:
				# Got something
				return false
		else:
			map_ptr += 1
	return true

# Returns false on EOF
func read_token() -> bool:
	if _seek_past_whitespace():
		return false
	var chr = map.ord_at(map_ptr)
	map_ptr += 1
	# It's worth noting we can't safely treat "{" as {.
	# There's a bunch of weird conventions in Half-Life slime textures and the like.
	# strings
	if chr == 34:
		current_token_symbol = false
		if permit_escapes:
			current_token_text = ""
			var escape = false
			while true:
				if map_ptr >= len(map):
					# just pretend it's all good
					break
				# note the type change here of chr, this is "fine"
				chr = map.substr(map_ptr, 1)
				map_ptr += 1
				if escape:
					current_token_text += chr
					escape = false
				elif chr == "\"":
					break
				elif chr == "\\":
					escape = true
				else:
					current_token_text += chr
		else:
			# If no escapes are permitted, it's simply a race to the first "
			var p2 = map.find("\"", map_ptr)
			if p2 == -1:
				p2 = len(map)
			current_token_text = map.substr(map_ptr, p2 - map_ptr)
			map_ptr = p2 + 1
		return true
	# long symbols, floats, etc.
	current_token_symbol = true
	# -1 for chr
	var cts = map_ptr - 1
	while map_ptr < len(map):
		if map.ord_at(map_ptr) <= 32:
			break
		map_ptr += 1
	current_token_text = map.substr(cts, map_ptr - cts)
	return true
