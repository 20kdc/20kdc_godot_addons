class_name MapIOMapReader
extends Reference

# Will return null
static func read_map(tokens: MapIOMapTokenStream) -> MapIODataMap:
	var map = MapIODataMap.new()
	while tokens.remaining():
		if tokens.is_symbol("{"):
			tokens.ptr += 1
			# Read entity!
			var entity = MapIODataEntity.new()
			while tokens.remaining():
				if tokens.is_symbol("}"):
					tokens.ptr += 1
					# End of entity
					break
				elif tokens.is_symbol("{"):
					tokens.ptr += 1
					# Brush
					var brush = MapIODataBrush.new()
					while tokens.remaining():
						if tokens.is_symbol("}"):
							# End of brush
							tokens.ptr += 1
							break
						elif tokens.is_symbol("("):
							# Start of face
							var point_a = Vector3(
								float(tokens.texts[tokens.ptr + 1]),
								float(tokens.texts[tokens.ptr + 2]),
								float(tokens.texts[tokens.ptr + 3])
							)
							# ) (
							var point_b = Vector3(
								float(tokens.texts[tokens.ptr + 6]),
								float(tokens.texts[tokens.ptr + 7]),
								float(tokens.texts[tokens.ptr + 8])
							)
							# ) (
							var point_c = Vector3(
								float(tokens.texts[tokens.ptr + 11]),
								float(tokens.texts[tokens.ptr + 12]),
								float(tokens.texts[tokens.ptr + 13])
							)
							# ) TEXTURE
							tokens.ptr += 16
							point_a = MapIOMathsBase.conv_q2g(point_a)
							point_b = MapIOMathsBase.conv_q2g(point_b)
							point_c = MapIOMathsBase.conv_q2g(point_c)
							var plane = Plane(point_a, point_b, point_c)
							# note this backreference to the texture
							# skips an increment
							var texture = tokens.texts[tokens.ptr - 1]
							if tokens.is_symbol("["):
								# Valve format
								var u = PoolRealArray([
									float(tokens.texts[tokens.ptr + 1]),
									float(tokens.texts[tokens.ptr + 2]),
									float(tokens.texts[tokens.ptr + 3]),
									float(tokens.texts[tokens.ptr + 4])
								])
								# skip ] [
								var v =  PoolRealArray([
									float(tokens.texts[tokens.ptr + 7]),
									float(tokens.texts[tokens.ptr + 8]),
									float(tokens.texts[tokens.ptr + 9]),
									float(tokens.texts[tokens.ptr + 10])
								])
								# skip ] R
								var scale_u = float(tokens.texts[tokens.ptr + 13])
								var scale_v = float(tokens.texts[tokens.ptr + 14])
								tokens.ptr += 15
								brush.add_plane(plane, texture, u, v, scale_u, scale_v)
							else:
								# Quake format
								var offset_u = float(tokens.texts[tokens.ptr])
								var offset_v = float(tokens.texts[tokens.ptr + 1])
								var angle = float(tokens.texts[tokens.ptr + 2])
								var scale_u = float(tokens.texts[tokens.ptr + 3])
								var scale_v = float(tokens.texts[tokens.ptr + 4])
								tokens.ptr += 5
								brush.add_old_plane(plane, texture, offset_u, offset_v, angle, scale_u, scale_v)
						else:
							# Note unhandled symbols are ignored,
							#  this is to eat extended (Q3) properties.
							tokens.ptr += 1
					entity.brushes.append(brush)
				else:
					# Property
					entity.keys[tokens.texts[tokens.ptr]] = tokens.texts[tokens.ptr + 1]
					tokens.ptr += 2
			map.entities.append(entity)
		else:
			tokens.ptr += 1
			push_warning("Unexpected token")
			return null
	return map
