extends MapIOEntityConverter
tool

func _mapio_convert_entity(entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	context.add_location(entity.vector("origin"), entity.string("name", "Room"), true, entity.integer("priority", 0))
	return null
