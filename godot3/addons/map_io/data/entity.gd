class_name MapIODataEntity
extends Resource
tool

# This maps strings to strings.
export var keys: Dictionary

# This is an array of MapIODataBrush.
export var brushes: Array

func _init():
	keys = {}
	brushes = []

func string(key: String, def: String = "") -> String:
	if keys.has(key):
		return keys[key]
	return def

func classname() -> String:
	return string("classname", "info_null")

func number(key: String, def: float = 0) -> float:
	if keys.has(key):
		return float(keys[key])
	return def

func integer(key: String, def: int = 0) -> int:
	if keys.has(key):
		return int(keys[key])
	return def

func vector(key: String, def: Vector3 = Vector3.ZERO) -> Vector3:
	if not keys.has(key):
		return def
	return vector_convert(keys[key])

func vector_nc(key: String, def: Vector3 = Vector3.ZERO) -> Vector3:
	if not keys.has(key):
		return def
	return vector_convert_nc(keys[key])

# note that this is translated via Godot semantics right now
# i.e. "1 1 1" not "255 255 255"
# need to look into this
func colour(key: String, def: Color = Color.white) -> Color:
	if not keys.has(key):
		return def
	var prl: PoolRealArray = keys[key].split_floats(" ", false)
	if len(prl) == 1:
		return Color(prl[0], prl[0], prl[0])
	if len(prl) == 3:
		return Color(prl[0], prl[1], prl[2])
	if len(prl) == 4:
		return Color(prl[0], prl[1], prl[2], prl[3])
	return def

static func vector_convert(v: String) -> Vector3:
	return MapIOMathsBase.conv_q2g(vector_convert_nc(v))

static func vector_convert_nc(v: String) -> Vector3:
	var prl: PoolRealArray = v.split_floats(" ", false)
	if len(prl) < 3:
		return Vector3.ZERO
	return Vector3(prl[0], prl[1], prl[2])

func angles() -> Vector3:
	if keys.has("angle"):
		return Vector3(0, float(keys["angle"]), 0)
	return vector_nc("angles")
