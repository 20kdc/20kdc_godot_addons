# serializable because Network needs the whole area_marker stuff
class_name MapIOAreaMarker
extends Reference

var name: String = ""
var location: Vector3 = Vector3.ZERO
var priority: int = 0
var explicit: bool = false

func read_from_stream_peer(sp: StreamPeer):
	name = sp.get_string()
	var x = sp.get_double()
	var y = sp.get_double()
	var z = sp.get_double()
	location = Vector3(x, y, z)
	priority = sp.get_64()
	explicit = sp.get_u8() != 0

func write_to_stream_peer(sp: StreamPeer):
	sp.put_string(name)
	sp.put_double(location.x)
	sp.put_double(location.y)
	sp.put_double(location.z)
	sp.put_64(priority)
	if explicit:
		sp.put_u8(1)
	else:
		sp.put_u8(0)
