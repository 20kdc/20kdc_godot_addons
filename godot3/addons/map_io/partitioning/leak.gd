class_name MapIOLeakTracer
extends Reference

static func trace_leak(location: Vector3, leaves: Array, graph: Dictionary) -> PoolVector3Array:
	# [leaf] = [winding_centre, leaf]
	var explored = {}
	var spread = []
	for l in leaves:
		var leaf: MapIOPartitioningLeaf = l
		if leaf.convex.point_side(location) <= MapIOMathsBase.SIDE_ON:
			explored[leaf] = [location, null]
			spread.push_back(leaf)
	while len(spread) > 0:
		var here: MapIOPartitioningLeaf = spread[0]
		spread.pop_front()
		if here.touches_void():
			# found void!
			var locations_trace = PoolVector3Array()
			while here != null:
				locations_trace.push_back(MapIOMathsBase.points_centre(here.convex.get_points()))
				# get intermediate winding that lead us here and such
				var interim: Array = explored[here]
				locations_trace.push_back(interim[0])
				here = interim[1]
			return locations_trace
		if graph.has(here):
			var c = graph[here]
			for l2 in c:
				if not explored.has(l2):
					# aha, a new leaf
					# attempt to determine winding centre
					var winding_a = here.convex.windings[c[l2]]
					var winding_b = l2.convex.windings[graph[l2][here]]
					var winding_c = MapIOMathsBase.windings_intersect_noplane_aswinding(winding_a, winding_b)
					var winding_centre = winding_a[0]
					if not winding_c.empty():
						winding_centre = MapIOMathsBase.points_centre(winding_c)
					explored[l2] = [winding_centre, here]
					spread.push_back(l2)
	# ---
	push_warning("abnormality - terminated without finding void")
	var locations = PoolVector3Array()
	locations.push_back(location)
	return locations
