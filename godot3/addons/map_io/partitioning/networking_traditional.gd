class_name MapIONetworkingTraditional
extends Reference

# Idea here is to spread from explicit areamarkers.
# As such, this skips portalizing the void.

# Sorts area_markers
static func network(leaves: Array, profiler: MapIOProfilerNull, graph_out: Dictionary, area_markers: Array) -> Array:
	# 1. Winding lookup
	profiler.profile_open("MapIONetworkingTraditional: creating winding lookup")
	var winding_lookup = MapIOPartitioningLeafConnector.generate_winding_lookup(leaves)
	profiler.profile_close()
	profiler.profile_statistic("MapIONetworkingTraditional: created winding lookup: " + winding_lookup.statistics())
	# 1a. leaf connector
	var leaf_connector = MapIOPartitioningLeafConnector.new()
	leaf_connector.winding_lookup = winding_lookup
	# 2. Inline leaf connection and other such stuff
	profiler.profile_open("MapIONetworkingTraditional: networking")
	# explicit areamarkers are ordered first to ensure they win the first-network rule
	area_markers.sort_custom(AreaMarkerExplicitSorting, "sortf")
	var networks = []
	var leaf_to_network = {}
	for am in area_markers:
		var marker: MapIOAreaMarker = am
		var loc = marker.location
		for v in leaves:
			if not leaf_to_network.has(v):
				var leaf: MapIOPartitioningLeaf = v
				if leaf.convex.point_side(loc) <= MapIOMathsBase.SIDE_ON:
					var network = _expand(leaf, leaf_connector, graph_out, leaf_to_network)
					network.area_marker = marker
					networks.push_back(network)
	profiler.profile_close()
	profiler.profile_statistic("MapIONetworkingTraditional: " + str(len(area_markers)) + " areamarkers, " + str(len(networks)) + " networks")
	return networks

static func _expand(leaf: MapIOPartitioningLeaf, leaf_connector: MapIOPartitioningLeafConnector, graph_out: Dictionary, leaf_to_network: Dictionary) -> MapIONetworkingNetwork:
	var network = MapIONetworkingNetworkInternal.new()
	# assign leaf
	network.leaves.push_back(leaf)
	leaf_to_network[leaf] = network
	# begin expansion
	var expand_queue = [leaf]
	while not expand_queue.empty():
		var v: MapIOPartitioningLeaf = expand_queue[0]
		expand_queue.pop_front()
		var connections = []
		leaf_connector.connect_leaf(v, connections, false)
		MapIOPartitioningLeafConnector.apply_connections_to_graph(connections, graph_out)
		for vx in connections:
			var b: MapIOPartitioningLeaf = vx[2]
			if not leaf_to_network.has(b):
				leaf_to_network[b] = network
				network.leaves.push_back(b)
				expand_queue.push_back(b)
	return network

class AreaMarkerExplicitSorting extends Reference:
	static func sortf(a: MapIOAreaMarker, b: MapIOAreaMarker):
		if a.explicit != b.explicit:
			return a.explicit
		# to assist in determinism
		return a.name < b.name
