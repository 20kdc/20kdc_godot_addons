class_name MapIONetworkingPockets
extends Reference

# graph_out format:
# graph[leaf1][leaf2] = winding
# note that this is leaves, not unique IDs as it was before
static func network(leaves: Array, profiler: MapIOProfilerNull, graph_out: Dictionary, area_markers: Array) -> Array:
	_connect_leaves(leaves, profiler, graph_out)
	return _leaves_to_networks(leaves, profiler, graph_out, area_markers)

static func _connect_leaves(leaves: Array, profiler: MapIOProfilerNull, graph_out: Dictionary):
	# 1. plane lookup
	profiler.profile_open("MapIONetworkingPockets: creating winding lookup")
	var winding_lookup = MapIOPartitioningLeafConnector.generate_winding_lookup(leaves)
	profiler.profile_close()
	profiler.profile_statistic("MapIONetworkingPockets: created winding lookup: " + winding_lookup.statistics())
	profiler.profile_open("MapIONetworkingPockets: connecting leaves")
	# 2. connections
	var connector = MapIOPartitioningLeafConnector.new()
	connector.winding_lookup = winding_lookup
	connector.run(leaves, graph_out)
	profiler.profile_close()

static func _leaves_to_networks(leaves: Array, profiler: MapIOProfilerNull, graph: Dictionary, area_markers: Array) -> Array:
	profiler.profile_open("MapIONetworkingPockets: networking")
	var networks = {}
	var checked = {}
	var check_queue = []
	for v in leaves:
		# Create a new network for each leaf.
		# Note that the queue ordering will guarantee no merges are required.
		check_queue.push_back([MapIONetworkingNetworkInternal.new(), v])
	while len(check_queue) > 0:
		# Grab entry from end
		var ptr = len(check_queue) - 1
		var cqe = check_queue[ptr]
		check_queue.remove(ptr)
		var network: MapIONetworkingNetworkInternal = cqe[0]
		var leaf: MapIOPartitioningLeaf = cqe[1]
		# Checked test
		if checked.has(leaf):
			continue
		checked[leaf] = network
		# Insert into network and confirm the network really exists
		network.leaves.push_back(leaf)
		networks[network] = true
		# Spread (New entries at end so they'll be processed first)
		if graph.has(leaf):
			for k in graph[leaf]:
				check_queue.push_back([network, k])
	profiler.profile_close()
	profiler.profile_statistic("MapIONetworkingPockets: " + str(len(networks.keys())) + " networks")
	profiler.profile_open("MapIONetworkingPockets: Area marker processing")
	# good optimization here would be to actually keep the BSP tree around
	# like, seriously, the BSP tree itself would just solve this basically instantly
	# but Traditional mode is going to rearrange this anyway, so who cares?
	for am in area_markers:
		var marker: MapIOAreaMarker = am
		var loc = marker.location
		for v in checked:
			var leaf: MapIOPartitioningLeaf = v
			var net: MapIONetworkingNetworkInternal = checked[v]
			var worth_full_check = false
			if net.area_marker != null:
				if marker.explicit and not net.area_marker.explicit:
					# explicit markers override non-explicit markers
					worth_full_check = true
			else:
				# if there's no marker, ideal to add one
				worth_full_check = true
			if worth_full_check:
				if leaf.convex.point_side(loc) <= MapIOMathsBase.SIDE_ON:
					net.area_marker = marker
	profiler.profile_close()
	return networks.keys()
