# NOTE: This class has serialization stuff involved!
# For further details you might want to look into caching_manager_null
class_name MapIONetworkingNetwork
extends Reference

# surfaces in this network
# note that areaportal surfaces *deliberately* go missing from this sometimes -
#  this is because an areaportal can only be in one network at a time safely.
# it can't be associated to no network (room) - Godot PVS won't have it.
# possible to duplicate them across rooms, but requires flipping data,
#  treating areaportals even more specially, etc.
# really not worth it, and may even cause complications.
# at best, worthy of "look into as a future option."
var surfaces: Array
var touches_void: bool = false
# stored area marker copy.
var area_marker: MapIOAreaMarker

func _init():
	surfaces = []

# Initialize this Network from the internal structure created during networking.
func from_network_internal(base: MapIONetworkingNetworkInternal):
	if base.area_marker != null:
		area_marker = base.area_marker
	var seen_surface = {}
	for v in base.leaves:
		var leaf: MapIOPartitioningLeaf = v
		# Void test
		if leaf.touches_void():
			touches_void = true
		var i = 0
		while i < len(leaf.convex.winding_tags):
			var tag = leaf.convex.winding_tags[i]
			if tag != null:
				for s in tag:
					if not seen_surface.has(s):
						var surface: MapIOSurfacingSurface = s
						# Areaportal check
						if surface.material.areaportal:
							if not surface.plane.is_equal_approx(leaf.convex.get_plane(i)):
								continue
						surfaces.push_back(s)
						seen_surface[s] = true
			i += 1

# Given an array of surfaces (the assumption is deterministic preparation + checksum), read a network from a peer.
func read_from_stream_peer(sp: StreamPeer, surfaces_source: Array):
	surfaces.resize(sp.get_u32())
	var i = 0
	while i < len(surfaces):
		surfaces[i] = surfaces_source[sp.get_u32()]
		i += 1
	touches_void = sp.get_u8() != 0
	if sp.get_u8() != 0:
		area_marker = MapIOAreaMarker.new()
		area_marker.read_from_stream_peer(sp)

# Given a dictionary from surface instances to IDs in an array, write that to a peer.
func write_to_stream_peer(sp: StreamPeer, surface_to_id: Dictionary):
	sp.put_u32(len(surfaces))
	for v in surfaces:
		sp.put_u32(surface_to_id[v])
	if touches_void:
		sp.put_u8(1)
	else:
		sp.put_u8(0)
	if area_marker != null:
		sp.put_u8(1)
		area_marker.write_to_stream_peer(sp)
	else:
		sp.put_u8(0)
