# Manager implementation that assumes a specific file is the correct file.
# In general, if you aren't reusing the config, you can just add this into it specifying the co-file location,
#  and it'll work.
class_name MapIONetworkingCachingManagerFile
extends MapIONetworkingCachingManagerBase

var location: String

func _init(l: String):
	location = l

func _mapio_caching_manager_cofile_read_impl(_input_hash: PoolByteArray) -> PoolByteArray:
	var f = File.new()
	if not f.file_exists(location):
		return PoolByteArray()
	if f.open(location, File.READ) != OK:
		return PoolByteArray()
	var res = f.get_buffer(f.get_len())
	f.close()
	return res

func _mapio_caching_manager_cofile_write_impl(_input_hash: PoolByteArray, data: PoolByteArray):
	var f = File.new()
	if f.open(location, File.WRITE) != OK:
		return
	f.store_buffer(data)
	f.close()
