class_name MapIOPartitioningHub
extends Reference

# Note: area_markers may be sorted
static func partition_and_portal_file(surfaces: Array, config: MapIOBuilderConfigLL, area_markers: Array):
	return _partition_and_portal_file_innards(surfaces, area_markers, config.partition_mode, config.networking_caching_manager, config.portal_file, config.debug_info, config.profiler, config.output_base_name)

# All meaningful options are at the start, all "unmeaningful" options afterwards.
# This is to help with caching manager stuff.
static func _partition_and_portal_file_innards(surfaces: Array, area_markers: Array, partition_mode: int, caching_manager: MapIONetworkingCachingManagerBase, portal_file: bool, debug_info: bool, profiler: MapIOProfilerNull, output_base_name: String):
	if partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.AUTOMATIC:
		if area_markers.empty():
			partition_mode = MapIOBuilderConfigSpecific.PARTITION_MODE.POCKETS_NO_VOID
			profiler.profile_statistic("MapIOPartitioningHub: AUTOMATIC decided on POCKETS_NO_VOID")
		else:
			partition_mode = MapIOBuilderConfigSpecific.PARTITION_MODE.TRADITIONAL_NO_VOID
			profiler.profile_statistic("MapIOPartitioningHub: AUTOMATIC decided on TRADITIONAL_NO_VOID")
	if partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.NONE:
		return _mode_none(surfaces)
	# now that we're sure of what we're doing, let's take the time to check the cache
	var caching_hash = PoolByteArray()
	if caching_manager != null:
		profiler.profile_open("MapIOPartitioningHub: Cache check")
		caching_hash = caching_manager.calculate_hash(surfaces, area_markers, partition_mode)
		var retrieved = caching_manager.attempt_cofile_read(surfaces, caching_hash, profiler)
		profiler.profile_close()
		profiler.profile_statistic("MapIOPartitioningHub: Is of hash: " + caching_hash.hex_encode())
		if retrieved != null:
			return retrieved
	# not caching or cache out of date, full steam ahead
	var leaves = MapIOPartitioning.partition(surfaces, profiler)
	var graph = {}
	var network_internals: Array
	if partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.TRADITIONAL_NO_VOID or partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.TRADITIONAL_INCLUDE_VOID:
		network_internals = MapIONetworkingTraditional.network(leaves, profiler, graph, area_markers)
	else:
		network_internals = MapIONetworkingPockets.network(leaves, profiler, graph, area_markers)
	# --- PAST THIS POINT, DELETE LEAVES - THIS REMOVES VOID LEAVES, WHICH WILL FREE UP POOLVECTOR MEMORY ---
	leaves = []
	# --- dump debug info that requires networks & graph around ---
	if portal_file:
		profiler.profile_open("MapIOPartitioningHub: portal file")
		MapIOMathsAux.save_windings_as_prt(output_base_name + ".prt", MapIOMathsAux.graph_to_stupidify_windings(graph))
		profiler.profile_close()
	if debug_info:
		profiler.profile_open("MapIOPartitioningHub: network debug info")
		MapIOMathsAux.save_network_internal_data(output_base_name, network_internals, graph)
		profiler.profile_close()
	# --- PAST THIS POINT, DELETE GRAPH - IN POCKETS MODE, THIS MIGHT FREE UP SOME STUFF, IN ANY CASE NO HARM DONE ---
	# we still need NetworkInternals so they can be transitioned to Networks, though
	graph = {}
	# --- NETWORK CONVERSION PROCESS WILL DELETE NETWORK_INTERNALS MEMBERS ---
	# Clean up networks and prepare metadata
	var networks_cleaned = []
	# Note:
	# 1. Areamarkers always preserve whatever they connect to, even if that includes the void
	# 2. If the void is intended to be removed, then if it isn't removed, that needs to be reported
	# 3. this is the bridge between "internal" network structures (lots of information we only need here),
	#     and "external" (massive loss of information but is in the format everything else needs)
	# 4. For memory management reasons, remove networks as they're converted
	var void_delete = (partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.POCKETS_NO_VOID) or (partition_mode == MapIOBuilderConfigSpecific.PARTITION_MODE.TRADITIONAL_NO_VOID)
	var reported_at_least_one_leak = false
	while len(network_internals) > 0:
		var network_internal: MapIONetworkingNetworkInternal = network_internals[0]
		network_internals.pop_front()
		var network = MapIONetworkingNetwork.new()
		network.from_network_internal(network_internal)
		if network.touches_void and void_delete:
			if network.area_marker == null:
				continue
			else:
				# area_marker leaked to void!!!
				var marker: MapIOAreaMarker = network.area_marker
				profiler.profile_statistic("MapIOPartitioningHub: *** LEAK IN AREA '" + marker.name + "' ***")
				if not reported_at_least_one_leak:
					# write out leakfile
					profiler.profile_open("MapIOPartitioningHub: Tracing leak")
					MapIOMathsAux.save_points_as_pts(output_base_name + ".pts", MapIOLeakTracer.trace_leak(marker.location, network_internal.leaves, graph))
					profiler.profile_close()
					reported_at_least_one_leak = true
		networks_cleaned.push_back(network)
	# --- PAST THIS POINT, NETWORK_INTERNALS IS EMPTY ---
	# now to return back to the caching
	if caching_manager != null:
		caching_manager.attempt_cofile_write(surfaces, caching_hash, networks_cleaned)
	return networks_cleaned

static func _mode_none(surfaces: Array) -> Array:
	var network = MapIONetworkingNetwork.new()
	# don't bother
	network.surfaces = surfaces
	return [network]
