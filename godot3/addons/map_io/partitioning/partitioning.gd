class_name MapIOPartitioning
extends Reference

# This no longer modifies the input array, mainly because it was causing fun issues to caching
static func partition(surfaces_input: Array, profiler: MapIOProfilerNull) -> Dictionary:
	var surfaces = surfaces_input.duplicate()
	# 1. optimize surface order
	surfaces.sort_custom(MapIOPartitioningSurfaceOrderOptimizer, "sort_func")
	# 2. create partitions
	profiler.profile_open("MapIOPartitioning: creating leaves")
	var initial_convex = MapIOMathsConvex.new()
	initial_convex.init_bigcube()
	var initial_leaf = MapIOPartitioningLeaf.new(initial_convex, surfaces)
	var leaves = []
	_process_leaf(initial_leaf, leaves)
	profiler.profile_close()
	return leaves

static func _process_leaf(leaf: MapIOPartitioningLeaf, leaves: Array):
	# Determine if there's anything to do
	if len(leaf.surfaces) == 0:
		leaf.unique_id = len(leaves)
		# NOTE: There's a very, very good reason this check exists:
		# It's possible for leaves to be created inside a wall.
		# Those leaves could then become their own rooms (VERY BAD).
		# So to prevent this they're deleted now.
		# Doing this early also reduces the load on the connectivity code by over 50%!
		if not _is_walled_off(leaf):
			leaf.reduce_winding_tags()
			leaves.push_back(leaf)
		return
	# Split time.
	var split: MapIOSurfacingSurface = leaf.surfaces[0]
	leaf.surfaces.remove(0)
	var plane = split.plane
	var plane_inv = -plane
	# Split surfaces between sides for proper plane propagation
	var surfaces_above = []
	var surfaces_below = []
	var surfaces_on = [split]
	for v in leaf.surfaces:
		var surf: MapIOSurfacingSurface = v
		var side = MapIOMathsBase.winding_plane_side(surf.winding, plane)
		if side == MapIOMathsBase.SIDE_ABOVE:
			surfaces_above.append(surf)
		elif side == MapIOMathsBase.SIDE_BELOW:
			surfaces_below.append(surf)
		else:
			# work out what to do with it
			var surf_plane = surf.plane
			if surf_plane.is_equal_approx(plane) or surf_plane.is_equal_approx(plane_inv):
				# actually part of the surface
				surfaces_on.append(surf)
			else:
				# not part of the surface but crosses both sides
				surfaces_above.append(surf)
				surfaces_below.append(surf)
	# The ordering here is paramount, as cut_plane uses the below convex as the primary (modified) object.
	var convex_above = MapIOMathsConvex.new()
	var convex_below = leaf.convex
	convex_below.cut_plane(plane, surfaces_on, convex_above, surfaces_on.duplicate())
	# Determine what goes on as a result.
	# Note that either of these being empty is normal if a cut plane was used that never actually hits.
	# In this case the cut plane (surfaces_on surfaces) are presumably lost, but that's okay.
	# It didn't hit anyway, did it?
	# Note that surfaces_on should guarantee that "neighbour" planes are caught.
	if not convex_above.empty():
		_process_leaf(MapIOPartitioningLeaf.new(convex_above, surfaces_above), leaves)
	if not convex_below.empty():
		_process_leaf(MapIOPartitioningLeaf.new(convex_below, surfaces_below), leaves)

static func _is_walled_off(leaf: MapIOPartitioningLeaf) -> bool:
	var i = 0
	while i < len(leaf.convex.windings):
		var winding = leaf.convex.windings[i]
		var tag = leaf.convex.winding_tags[i]
		var plane = leaf.convex.get_plane(i)
		if tag == null:
			return false
		for v in tag:
			var surf: MapIOSurfacingSurface = v
			if not (surf.material.window or surf.material.areaportal):
				if surf.plane.is_equal_approx(plane):
					if MapIOMathsBase.windings_contained_noplane(surf.winding, winding):
						return true
		i += 1
	return false
