class_name MapIOPartitioningSurfaceOrderOptimizer
extends Reference

static func sort_func(a: MapIOSurfacingSurface, b: MapIOSurfacingSurface):
	# Highest priority: Areaportals always go first.
	# This is in an attempt to isolate most unrelated splits.
	# In particular this ought to reduce the amount of non-axis-aligned splits.
	if a.material.areaportal != b.material.areaportal:
		return a.material.areaportal
	# Second priority: Axis-alignment always wins.
	if a.axis_aligned != b.axis_aligned:
		return a.axis_aligned
	# Third priority: Surface area.
	return a.importance_heuristic > b.importance_heuristic
