# Contains utilities for converting common structures into inefficient forms.
# Said forms are less likely to cause godot#54263 to act up.
class_name MapIOMathsStupidifyComplex
extends Reference

static func destupidify_array_of_convexes(a: Array) -> Array:
	var b = []
	for v in a:
		var convex = MapIOMathsConvex.new()
		convex.init_stupidify(v)
		b.push_back(convex)
	return b
