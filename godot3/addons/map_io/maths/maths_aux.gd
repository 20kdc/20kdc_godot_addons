# CALLED maths_aux.gd BECAUSE WINDOWS IS STUPID
# Auxilliary utilties
# In practice, debug stuff
class_name MapIOMathsAux
extends Reference

static func convex_to_debug_mesh(c: MapIOMathsConvex) -> ArrayMesh:
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_LINES)
	for w in c.windings:
		var last_vertex: Vector3 = w[len(w) - 1]
		var nudge = MapIOMathsBase.winding_plane(w).normal * 2
		for v in w:
			st.add_color(Color.red)
			st.add_vertex(last_vertex + nudge)
			st.add_color(Color.green)
			st.add_vertex(v + nudge)
			last_vertex = v
	return st.commit()

static func _connections(l: MapIOPartitioningLeaf, connections: Dictionary):
	if connections.has(l):
		return connections[l]
	return {}

# Deliberately uses "stupidified" (avoiding PoolVector3) windings
# This is because, say, Quake E1M1 starts causing PoolVector errors
static func graph_to_stupidify_windings(graph: Dictionary) -> Array:
	var array = []
	var covered = {}
	for vp in graph.keys():
		var leaf1: MapIOPartitioningLeaf = vp
		var leaf1c: Dictionary = graph[leaf1]
		for k2 in leaf1c:
			var leaf2: MapIOPartitioningLeaf = k2
			var leaf2c: Dictionary = _connections(leaf2, graph)
			if covered.has(str(leaf1.unique_id) + "_" + str(leaf2.unique_id)):
				continue
			covered[str(leaf1.unique_id) + "_" + str(leaf2.unique_id)] = true
			covered[str(leaf2.unique_id) + "_" + str(leaf1.unique_id)] = true
			var leaf1w: int = leaf1c[leaf2]
			var leaf2w: int = leaf2c[leaf1]
			var res = MapIOMathsBase.windings_intersect_noplane_aswinding(leaf1.convex.windings[leaf1w], leaf2.convex.windings[leaf2w])
			if len(res) > 0:
				array.push_back(Array(res))
	return array

static func save_windings_as_prt(name: String, windings: Array):
	# generate PRT file
	var prt = File.new()
	prt.open(name, File.WRITE)
	prt.store_line("PRT1")
	prt.store_line("1")
	prt.store_line(str(len(windings)))
	for winding in windings:
		var ln = str(len(winding)) + " 0 0"
		for pt in winding:
			ln += " " + format_quake_vector(pt)
		prt.store_line(ln)
	prt.close()

static func save_points_as_pts(name: String, points: PoolVector3Array):
	# generate PTS file
	var pts = File.new()
	pts.open(name, File.WRITE)
	for pt in points:
		pts.store_line(format_quake_vector_np(pt))
	pts.close()

static func format_quake_vector(pt: Vector3) -> String:
	return "( " + format_quake_vector_np(pt) + " )"

static func format_quake_vector_np(pt: Vector3) -> String:
	var pta = MapIOMathsBase.conv_g2q(pt)
	return str(pta.x) + " " + str(pta.y) + " " + str(pta.z)

static func save_network_internal_data(name: String, networks: Array, graph: Dictionary):
	_save_network_data_map(name, networks, graph)
	# Save report
	var txt = File.new()
	txt.open(name + ".networking.txt.prt", File.WRITE)
	txt.store_line(" - Leaf/Surface Report - ")
	var network_index = 0
	for n in networks:
		txt.store_line(" - Network " + str(network_index )+ " - ")
		network_index += 1
		var network: MapIONetworkingNetworkInternal = n
		for l in network.leaves:
			var leaf1: MapIOPartitioningLeaf = l
			var leaf1c = _connections(leaf1, graph)
			txt.store_line("Leaf " + str(leaf1.unique_id) + ":")
			var idx = 0
			for tag in leaf1.convex.winding_tags:
				if tag != null:
					txt.store_line(" Leaf " + str(leaf1.unique_id) + ", Winding " + str(idx) + ":")
					for s in tag:
						var surf: MapIOSurfacingSurface = s
						var cstat = "  " + surf.brush.textures[surf.face_index]
						cstat += " (" + str(surf.get_instance_id()) + ")"
						var wi = MapIOMathsBase.windings_intersect_noplane_aswinding(surf.winding, leaf1.convex.windings[idx])
						if len(wi) == 0:
							cstat += " ABNORMAL!"
							if MapIOMathsBase.windings_intersect_noplane(surf.winding, leaf1.convex.windings[idx]):
								cstat += " CONFLICTS W/ windings_intersect_noplane!"
							else:
								cstat += " windings_intersect_noplane CONCURS!"
						txt.store_line(cstat)
				else:
					txt.store_line(" Winding " + str(idx) + " - Void")
				for k2 in leaf1c:
					var leaf2: MapIOPartitioningLeaf = k2
					if leaf1c[k2] == idx:
						txt.store_line("  connects to leaf " + str(leaf2.unique_id))
				idx += 1
	txt.close()
static func _save_network_data_map(name: String, networks: Array, graph: Dictionary):
	# generate MAP file the fun way!
	var prt = File.new()
	prt.open(name + ".networking.map.prt", File.WRITE)
	prt.store_line("// Game: MapIO")
	prt.store_line("// Format: Standard")
	prt.store_line("{")
	prt.store_line(" \"classname\" \"worldspawn\"")
	prt.store_line(" \"_tb_textures\" \"textures;textures/lab;textures/tools\"")
	prt.store_line(" \"_tb_mod\" \"baseq3\"")
	prt.store_line("}")
	var tbid = 1
	var network_index = 0
	for n in networks:
		var network: MapIONetworkingNetworkInternal = n
		prt.store_line("{")
		prt.store_line(" \"classname\" \"func_group\"")
		prt.store_line(" \"_tb_type\" \"_tb_group\"")
		prt.store_line(" \"_tb_name\" \"Network " + str(network_index) + "\"")
		network_index += 1
		var net_tbid = tbid
		prt.store_line(" \"_tb_id\" \"" + str(net_tbid) + "\"")
		tbid += 1
		prt.store_line("}")
		for l in network.leaves:
			var leaf: MapIOPartitioningLeaf = l
			var leafc = _connections(leaf, graph)
			prt.store_line("{")
			prt.store_line(" \"classname\" \"func_group\"")
			prt.store_line(" \"_tb_type\" \"_tb_group\"")
			prt.store_line(" \"_tb_name\" \"Leaf " + str(leaf.unique_id) + "\"")
			prt.store_line(" \"_tb_id\" \"" + str(tbid) + "\"")
			tbid += 1
			prt.store_line(" \"_tb_group\" \"" + str(net_tbid) + "\"")
			var connected_windings = {}
			for k in leafc:
				connected_windings[leafc[k]] = true
			prt.store_line("{")
			var i = 0
			while i < len(leaf.convex.windings):
				var a = leaf.convex.windings[i][0]
				var b = leaf.convex.windings[i][1]
				var c = leaf.convex.windings[i][2]
				var tex = "tools/skip"
				if connected_windings.has(i):
					tex = "tools/areaportal"
				var ln = format_quake_vector(a) + " " + format_quake_vector(b) + " " + format_quake_vector(c) + " " + tex + " 0 0 0 1 1"
				prt.store_line(ln)
				i += 1
			prt.store_line("}")
			prt.store_line("}")
	prt.close()
