# Contains utilities for converting common structures into inefficient forms.
# Said forms are less likely to cause godot#54263 to act up.
class_name MapIOMathsStupidify
extends Reference

static func stupidify_array_of_poolvector(a: Array) -> Array:
	var b = []
	for v in a:
		b.push_back(Array(v))
	return b

static func destupidify_array_of_poolvector3(a: Array) -> Array:
	var b = []
	for v in a:
		b.push_back(PoolVector3Array(v))
	return b

static func stupidify_array_with_callback(a: Array) -> Array:
	var b = []
	for v in a:
		b.push_back(v.stupidify())
	return b
