# Oooh, this gets complicated.
extends Reference
class_name MapIOMathsConvex

# see: base.gd for winding maths
# array of PoolVector3Array
export var windings: Array
export var winding_tags: Array
export var normals: PoolVector3Array
export var distances: PoolRealArray

func _init():
	clear()

func clear():
	windings = []
	winding_tags = []
	normals = PoolVector3Array()
	distances = PoolRealArray()

func init_bigcube():
	init_cube(65536)

func init_cube(s: float):
	# Build a cube!
	var p = [
		#        X   Y   Z
		Vector3(-s, -s, -s),
		Vector3(-s, -s,  s),
		Vector3(-s,  s, -s),
		Vector3(-s,  s,  s),
		Vector3( s, -s, -s),
		Vector3( s, -s,  s),
		Vector3( s,  s, -s),
		Vector3( s,  s,  s)
	]
	windings.push_back(PoolVector3Array([p[5], p[7], p[6], p[4]])) # X+ 0
	normals.push_back(Vector3.RIGHT)
	windings.push_back(PoolVector3Array([p[3], p[2], p[6], p[7]])) # Y+ 1
	normals.push_back(Vector3.UP)
	windings.push_back(PoolVector3Array([p[1], p[3], p[7], p[5]])) # Z+ 2
	normals.push_back(Vector3.BACK)
	windings.push_back(PoolVector3Array([p[1], p[0], p[2], p[3]])) # X- 3
	normals.push_back(Vector3.LEFT)
	windings.push_back(PoolVector3Array([p[0], p[1], p[5], p[4]])) # Y- 4
	normals.push_back(Vector3.DOWN)
	windings.push_back(PoolVector3Array([p[2], p[0], p[4], p[6]])) # Z- 5
	normals.push_back(Vector3.FORWARD)
	var i = 0
	while i < 6:
		distances.push_back(s)
		winding_tags.push_back(null)
		# SANITY CHECK
		if false:
			MapIOMathsBase.winding_check(windings[i], normals[i])
		i += 1

func init_copy(other):
	windings.append_array(other.windings)
	winding_tags.append_array(other.winding_tags)
	normals.append_array(other.normals)
	distances.append_array(other.distances)

func init_planes(planes: Array, wt: Array):
	var idx = 0
	while idx < len(planes):
		var plane: Plane = planes[idx]
		var winding = MapIOMathsBase.winding_big_from_plane(plane)
		# clip winding
		var idx2 = 0
		while idx2 < len(planes):
			if idx != idx2:
				winding = Geometry.clip_polygon(winding, planes[idx2])
			idx2 += 1
		# sanity check - Geometry.clip_polygon is careful to make sure inside points must exist,
		#  and will return an empty array if not, but that's no good if we don't actually *check*
		if len(winding) > 2:
			_add_winding(winding, wt[idx], plane.normal, plane.d)
		idx += 1
	# determine if the solid has collapsed
	if len(windings) < 4:
		clear()

func init_stupidify(stupidify: Array):
	windings = MapIOMathsStupidify.destupidify_array_of_poolvector3(stupidify[0])
	winding_tags = stupidify[1]
	normals = PoolVector3Array(stupidify[2])
	distances = PoolRealArray(stupidify[3])

# Creates a (partially-shared-structure!) representation of the Convex.
# This doesn't take up as much PoolVector3Array room.
func stupidify():
	return [
		MapIOMathsStupidify.stupidify_array_of_poolvector(windings),
		winding_tags,
		Array(normals),
		Array(distances)
	]

func _add_winding(w: PoolVector3Array, tag, n: Vector3, d: float):
	windings.append(w)
	winding_tags.append(tag)
	normals.append(n)
	distances.append(d)

func _remove_winding(i: int):
	windings.remove(i)
	winding_tags.remove(i)
	normals.remove(i)
	distances.remove(i)

# Removes everything above the plane.
func cut_plane(p: Plane, tag, above_c = null, above_tag = null):
	var cutpoints = PoolVector3Array()
	var i = 0
	while i < len(windings):
		var v = windings[i]
		var res = MapIOMathsBase.winding_split(v, p)
		var below = res[1]
		var above = res[0]
		if (above != null) and (above_c != null):
			above_c._add_winding(above, winding_tags[i], normals[i], distances[i])
		cutpoints.append_array(res[2])
		# final decision to keep/remove winding
		if below != null:
			windings[i] = below
			i += 1
		else:
			_remove_winding(i)
	# clean up cutpoints
	cutpoints = MapIOMathsBase.unique_points(cutpoints)
	if len(cutpoints) > 2:
		var cutwinding = MapIOMathsBase.winding_from_points(cutpoints, p.normal)
		_add_winding(cutwinding, tag, p.normal, p.d)
		if above_c != null:
			cutwinding.invert()
			above_c._add_winding(cutwinding, above_tag, -p.normal, -p.d)
	# determine if the solid has collapsed
	if len(windings) < 4:
		clear()
	if above_c != null:
		if len(above_c.windings) < 4:
			above_c.clear()

func plane_would_intersect(p: Plane) -> bool:
	for v in windings:
		if MapIOMathsBase.winding_plane_would_intersect(v, p):
			return true
	return false

func point_side(p: Vector3) -> int:
	var i = 0
	var res = MapIOMathsBase.SIDE_ON
	while i < len(normals):
		var side = MapIOMathsBase.dot_side(MapIOMathsBase.point_plane_dot(p, Plane(normals[i], distances[i])))
		if side > MapIOMathsBase.SIDE_ON:
			return MapIOMathsBase.SIDE_ABOVE
		elif side == MapIOMathsBase.SIDE_BELOW:
			res = MapIOMathsBase.SIDE_BELOW
		i += 1
	return res

func get_plane(i: int) -> Plane:
	return Plane(normals[i], distances[i])

func get_planes() -> Array:
	var planes = []
	var i = 0
	while i < len(normals):
		planes.push_back(Plane(normals[i], distances[i]))
		i += 1
	return planes

func find_plane(p: Plane) -> int:
	# Find existing plane
	var i = 0
	while i < len(windings):
		var winding_plane = Plane(normals[i], distances[i])
		if winding_plane.is_equal_approx(p):
			# Plane is equal to or inverted to target.
			return i
		i += 1
	return -1

# Get unique points
func get_points() -> PoolVector3Array:
	var points = PoolVector3Array()
	for w in windings:
		points.append_array(w)
	return MapIOMathsBase.unique_points(points)

func empty() -> bool:
	return windings.empty()
