class_name MapIOFGDEnums
extends Reference

# Synced to mapio.fgd as "body_collision" key
enum COLLISION_TYPE {
	CONVEXES,
	BOXES,
	CONCAVES,
	NONE
}
const COLLISION_TYPE_HINT = "Convexes,Boxes,Concaves,None"

# Synced to mapio.fgd as "body_type" key
enum BODY_TYPE {
	KINEMATIC,
	STATIC,
	RIGID,
	AREA,
	SPATIAL,
	RIGID_CHARACTER,
	KINEMATIC_SYNC_TO_PHYSICS
}
const BODY_TYPE_HINT = "KinematicBody,StaticBody,RigidBody (Rigid Mode - like func_physbox),Area,Spatial (No collision),RigidBody (Character Mode - like func_pushable),KinematicBody (Sync-To-Physics - animatable in-editor)"

# Synced to mapio.fgd as "rendering" key
enum RENDERING_TYPE {
	DYNAMIC,
	WORLD_LIGHTING,
	NONE
}
