# Low-level builder config.
# Also contains the material cache.
class_name MapIOBuilderConfigLL
extends Reference

# -- Shared with low-level & specific config --
var mode = MapIOBuilderConfigSpecific.MODE.MULTI_ROOM
var partition_mode = MapIOBuilderConfigSpecific.PARTITION_MODE.AUTOMATIC
var prop_type = MapIOFGDEnums.BODY_TYPE.STATIC
var world_collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
var world_use_in_baked_light = true
var world_generate_lightmap = true
var lightmap_texel_size = 16.0
var portal_file = false
var debug_info = false
var output_base_name = "_runtime_info"
# -- Shared with high-level config --
var scale_divisor = 64.0
# Worth noting: Origin brushes are handled in the brush entity builder.
# This is why the name assignment exists in the low level builder.
var texture_origin = "tools/origin"
# --
var profiler: MapIOProfilerNull
var networking_caching_manager: MapIONetworkingCachingManagerBase = null

# String -> MapIOBuilderMaterial (no nulls!)
# Mandatory to set!
var material_lookup: FuncRef

# Cache of materials (implemented here for simplicity)
var material_cache: Dictionary

func copy_basics(from: MapIOBuilderConfigSpecific):
	mode = from.mode
	partition_mode = from.partition_mode
	prop_type = from.prop_type
	world_collision = from.world_collision
	world_use_in_baked_light = from.world_use_in_baked_light
	world_generate_lightmap = from.world_generate_lightmap
	lightmap_texel_size = from.lightmap_texel_size
	portal_file = from.portal_file
	debug_info = from.debug_info
	output_base_name = from.output_base_name
	if from.debug_profile:
		profiler = MapIOProfilerPrint.new()
	else:
		profiler = MapIOProfilerNull.new()

func copy_basics2(from: MapIOBuilderConfig):
	scale_divisor = from.scale_divisor
	texture_origin = from.texture_origin

func get_material(mat: String) -> MapIOBuilderMaterial:
	if material_cache.has(mat):
		return material_cache[mat]
	var ml: MapIOBuilderMaterial = material_lookup.call_func(mat)
	material_cache[mat] = ml
	return ml

