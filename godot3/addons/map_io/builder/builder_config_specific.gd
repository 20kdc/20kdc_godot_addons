# Highest-level builder config.
class_name MapIOBuilderConfigSpecific
extends Reference

# Remember: Keep the import plugin up to date with this!!!

enum MODE {
	SINGLE_ROOM, # a single Room w/ areaportals
	MULTI_ROOM, # split into rooms based on areaportals
	MULTI_ROOM_WITH_MANAGER, # MULTI_ROOM but with a RoomManager setup for maximum joy
	PROP # "prop" - compile "as if" via the default entity pipeline, see prop_type
}

const MODE_HINT = "Single Room,Multi-Room,Multi-Room With RoomManager,Prop"

enum PARTITION_MODE {
	NONE,
	AUTOMATIC,
	POCKETS_NO_VOID,
	POCKETS_INCLUDE_VOID,
	TRADITIONAL_NO_VOID,
	TRADITIONAL_INCLUDE_VOID
}

const PARTITION_MODE_HINT = "None (Skips partitioning while maintaining structure - fast but lazy),Automatic (Traditional if areamarkers are found or Pockets otherwise),Pockets (Can be glitchy and slow but doesn't require areamarkers),Pockets With Void,Traditional (Fastest and safest but requires areamarkers),Traditional With Void"

# -- Shared with low-level & specific config --
var mode = MODE.MULTI_ROOM
var partition_mode = PARTITION_MODE.AUTOMATIC
var prop_type = MapIOFGDEnums.BODY_TYPE.STATIC
var world_collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
var world_use_in_baked_light = true
var world_generate_lightmap = true
var lightmap_texel_size = 16.0
var portal_file = false
var debug_info = false
var output_base_name = "_runtime_info"
# "kinda" shared as "profiler" - honestly may be altered for profiler refactoring
# alternatively BuilderConfigInstance might just pass over the output_base_name
# in a sense, this would solve the problem
var debug_profile = false
# -- Handled by MapIOBuilderConfigInstance --
var builder_config = "res://default_map_io_config.tres"
var builder_config_fallback = "res://addons/map_io/config_copy_me/default_map_io_config.tres"
var entity_config = "res://default_map_io_entity_config.tres"
var entity_config_fallback = "res://addons/map_io/config_copy_me/default_map_io_entity_config.tres"
