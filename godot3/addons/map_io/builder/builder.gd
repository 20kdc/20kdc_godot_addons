# The map builder.
class_name MapIOBuilder
extends Reference

static func build(map: MapIODataMap, config: MapIOBuilderConfigLL, converter: MapIOEntityConverter) -> Spatial:
	# Setup entity context
	var entity_context: MapIOEntityConverterContext = MapIOEntityConverterContext.new()
	entity_context.config = config
	# Handle entities (this accumulates context data, may be needed by later stages at some point)
	var entity_nodes = []
	config.profiler.profile_open("MapIOBuilder: entities")
	var i = 1
	while i < len(map.entities):
		var ent = map.entities[i]
		var entnode = converter._mapio_convert_entity(ent, entity_context)
		if entnode != null:
			entity_nodes.push_back(entnode)
		i += 1
	config.profiler.profile_close()
	# Prepare root
	var root: Spatial
	var worldspawn: MapIODataEntity = map.entities[0]
	if config.mode == MapIOBuilderConfigSpecific.MODE.PROP:
		# Props follow the default brush entity pipeline
		# However, origin guess is not allowed
		root = build_prop(MapIOBuilderEntityPlaceholder, worldspawn, config, config.prop_type, MapIOFGDEnums.RENDERING_TYPE.WORLD_LIGHTING, config.world_collision, false)
		# clear transform so that defaults are "honest"
		root.transform = Transform.IDENTITY
	elif config.mode == MapIOBuilderConfigSpecific.MODE.SINGLE_ROOM:
		# Single-room pipeline: Entities are part of the room for design simplicity,
		#  you're expected to extract them yourself
		root = build_single_room(worldspawn, config, entity_context.area_markers)
	elif config.mode == MapIOBuilderConfigSpecific.MODE.MULTI_ROOM:
		# Multi-room pipeline
		root = build_multi_room(worldspawn, config, entity_context.area_markers)
	elif config.mode == MapIOBuilderConfigSpecific.MODE.MULTI_ROOM_WITH_MANAGER:
		root = RoomManager.new()
		var roomlist = build_multi_room(worldspawn, config, entity_context.area_markers)
		roomlist.name = "rooms"
		root.add_child(roomlist)
		root.roomlist = "rooms"
	else:
		push_warning("MapIOBuilder: Unknown mode!")
		root = Spatial.new()
	# Add in entity nodes
	for v in entity_nodes:
		root.add_child(v)
	return root

static func build_single_room(worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL, area_markers: Array) -> Room:
	var tagged_convexes = _worldspawn_brushes_to_tagged_convexes(worldspawn.brushes, config)
	var surfaces = _tagged_convexes_to_surfaces(tagged_convexes, config)
	# Workaround for lack of godot#54263 - hold tagged convexes in stasis
	var tagged_convexes_stasis = MapIOMathsStupidify.stupidify_array_with_callback(tagged_convexes)
	tagged_convexes = []
	# Partitioning
	var networks = MapIOPartitioningHub.partition_and_portal_file(surfaces, config, area_markers)
	var surfaces_dict = {}
	var surfaces_void_dict = {}
	config.profiler.profile_open("MapIOBuilder: room-building")
	for n in networks:
		var net: MapIONetworkingNetwork = n
		if not net.touches_void:
			for v in net.surfaces:
				surfaces_dict[v] = true
		else:
			for v in net.surfaces:
				surfaces_void_dict[v] = true
	surfaces = surfaces_dict.keys()
	var surfaces_void = surfaces_void_dict.keys()
	# Continue as normal
	var room = MapIOBuilderLL.build_single_room_core(surfaces, config)
	room.name = "room"
	if not surfaces_void.empty():
		var void_room = MapIOBuilderLL.build_single_room_core(surfaces_void, config)
		void_room.name = "_void_room"
		room.add_child(void_room)
	config.profiler.profile_close()
	_attach_collision_and_data(room, worldspawn, tagged_convexes_stasis, config)
	return room

static func build_multi_room(worldspawn: MapIODataEntity, config: MapIOBuilderConfigLL, area_markers: Array) -> Spatial:
	var tagged_convexes = _worldspawn_brushes_to_tagged_convexes(worldspawn.brushes, config)
	var surfaces = _tagged_convexes_to_surfaces(tagged_convexes, config)
	# Workaround for lack of godot#54263 - hold tagged convexes in stasis
	var tagged_convexes_stasis = MapIOMathsStupidify.stupidify_array_with_callback(tagged_convexes)
	tagged_convexes = []
	# Partitioning section
	var networks = MapIOPartitioningHub.partition_and_portal_file(surfaces, config, area_markers)
	# Continue as normal
	var roomlist = Spatial.new()
	roomlist.set_display_folded(true)
	config.profiler.profile_open("MapIOBuilder: room-building")
	for n in networks:
		var net: MapIONetworkingNetwork = n
		var room = MapIOBuilderLL.build_single_room_core(net.surfaces, config)
		var priority = 0
		# set defaults for void
		if net.touches_void:
			room.name = "_void_room"
			priority = -1
		# apply area marker details
		if net.area_marker != null:
			room.name = net.area_marker.name
			if not net.touches_void:
				priority = net.area_marker.priority
		# continue
		if priority != 0:
			var rg = RoomGroup.new()
			# copy the name from the room
			rg.name = room.name
			rg.roomgroup_priority = priority
			rg.add_child(room)
			roomlist.add_child(rg)
		else:
			roomlist.add_child(room)
	config.profiler.profile_close()
	# Collision & data uses stasis version
	_attach_collision_and_data(roomlist, worldspawn, tagged_convexes_stasis, config)
	return roomlist

# Used for 
# Called from:
#  builder.gd (for props)
#  entity_converter_simple.gd (for entities that don't need special handling)
# body_type is BODY_TYPE
# rendering is RENDERING_TYPE
# collision is COLLISION_TYPE
static func build_prop(script: Script, entity: MapIODataEntity, config: MapIOBuilderConfigLL, body_type: int, rendering: int, collision: int, origin_guess_allowed: bool):
	var root: Spatial
	if body_type == MapIOFGDEnums.BODY_TYPE.KINEMATIC:
		root = KinematicBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.KINEMATIC_SYNC_TO_PHYSICS:
		root = KinematicBody.new()
		# Sync to physics is set later since Godot doesn't like these being in-tree
	elif body_type == MapIOFGDEnums.BODY_TYPE.STATIC:
		root = StaticBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.RIGID:
		root = RigidBody.new()
	elif body_type == MapIOFGDEnums.BODY_TYPE.RIGID_CHARACTER:
		root = RigidBody.new()
		root.mode = RigidBody.MODE_CHARACTER
	elif body_type == MapIOFGDEnums.BODY_TYPE.AREA:
		root = Area.new()
	else:
		if body_type != MapIOFGDEnums.BODY_TYPE.SPATIAL:
			push_warning("_mapio_convert_entity: found unknown body_type " + str(body_type))
		root = Spatial.new()
		collision = MapIOFGDEnums.COLLISION_TYPE.NONE
	MapIOBuilderLL.build_entity(root, entity, config, rendering, collision, origin_guess_allowed)
	root.set_script(script)
	MapIOBuilderLL.attach_entity_data(root, entity, config)
	if body_type == MapIOFGDEnums.BODY_TYPE.KINEMATIC_SYNC_TO_PHYSICS:
		root.set_sync_to_physics(true)
		# Sync to physics is set later since Godot doesn't like these being in-tree
	return root

static func _worldspawn_brushes_to_tagged_convexes(brushes: Array, config: MapIOBuilderConfigLL):
	config.profiler.profile_open("MapIOBuilder: brush clipping")
	var res = MapIOSurfacing.brushes_to_tagged_convexes(brushes)
	config.profiler.profile_close()
	return res

static func _tagged_convexes_to_surfaces(tagged_convexes: Array, config: MapIOBuilderConfigLL):
	config.profiler.profile_open("MapIOBuilder: surfacing")
	var surfaces = MapIOSurfacing.tagged_convexes_to_surfaces(tagged_convexes, config)
	config.profiler.profile_close()
	return surfaces

static func _attach_collision_and_data(target: Spatial, worldspawn: MapIODataEntity, worldspawn_tagged_convexes_stasis: Array, config: MapIOBuilderConfigLL):
	# bring stuff out of stasis
	config.profiler.profile_open("MapIOBuilder: pull convexes out of memory stasis to work around godot#37154")
	var tagged_convexes = MapIOMathsStupidifyComplex.destupidify_array_of_convexes(worldspawn_tagged_convexes_stasis)
	worldspawn_tagged_convexes_stasis = []
	config.profiler.profile_close()
	# ---
	config.profiler.profile_open("MapIOBuilder: collision/data")
	target.set_script(MapIOBuilderEntityPlaceholder)
	MapIOBuilderLL.attach_entity_data(target, worldspawn, config)
	var staticbody = StaticBody.new()
	staticbody.set_display_folded(true)
	target.add_child(staticbody)
	MapIOBuilderLL.build_collision(staticbody, worldspawn.brushes, tagged_convexes, Transform.IDENTITY.scaled(Vector3.ONE / config.scale_divisor), config, config.world_collision)
	config.profiler.profile_close()

static func update_owners(owner: Node, target: Node):
	if target.owner != null:
		return
	if target != owner:
		target.owner = owner
	for v in target.get_children():
		update_owners(owner, v)
