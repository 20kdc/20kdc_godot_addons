# The map builder.
class_name MapIOBuilderLL
extends Reference

# Surfaces is an array of MapIOSurfacingSurface.
static func build_single_room_core(surfaces: Array, config: MapIOBuilderConfigLL) -> Room:
	var room = Room.new()
	var r = build_rendering(surfaces, config, config.world_use_in_baked_light, config.world_generate_lightmap, CullInstance.PORTAL_MODE_STATIC)
	room.add_child(r)
	return room

# Called from:
#  entity_converter_simple.gd
#  MapIOBuilder.build_prop
#  MapIOBuilder._attach_collision_and_data
static func attach_entity_data(root: Spatial, entity: MapIODataEntity, config: MapIOBuilderConfigLL):
	# This is writing into the extended script.
	root.scale_divisor = config.scale_divisor
	root.entity_resource = entity
	if entity.keys.has("targetname"):
		root.name = entity.keys["targetname"]
	if entity.keys.has("groups"):
		var groups: String = entity.keys["groups"]
		for v in groups.split(",", true):
			root.add_to_group(v, true)
	if entity.keys.has("visible"):
		root.visible = entity.keys["visible"] != "0"
	if root is CollisionObject:
		root.collision_layer = entity.integer("collision_layer", 1)
		root.collision_mask = entity.integer("collision_mask", 1)

# Responsible for:
#  + Determining + applying Origin/Angles
#  + Rendering & Collision
# Essentially the core of the handling for a brush entity.
# Called from MapIOBuilder.build_entity_root
# rendering is RENDERING_TYPE
# collision is COLLISION_TYPE
static func build_entity(root: Spatial, entity: MapIODataEntity, config: MapIOBuilderConfigLL, rendering: int, collision: int, origin_guess_allowed: bool):
	var has_brushes = not entity.brushes.empty()
	var brush_model: Spatial = null
	var tagged_convexes: Array = MapIOSurfacing.brushes_to_tagged_convexes(entity.brushes)
	if has_brushes and (rendering != MapIOFGDEnums.RENDERING_TYPE.NONE):
		var surfaces = MapIOSurfacing.tagged_convexes_to_surfaces(tagged_convexes, config)
		var bl = false
		var lm = false
		if rendering == MapIOFGDEnums.RENDERING_TYPE.WORLD_LIGHTING:
			bl = config.world_use_in_baked_light
			lm = config.world_generate_lightmap
		brush_model = build_rendering(surfaces, config, bl, lm, CullInstance.PORTAL_MODE_ROAMING)
		root.add_child(brush_model)
	# origin/angle handling
	determine_transform(root, entity, config, origin_guess_allowed)
	var ora_inverse = root.transform.inverse()
	# brush model inversion handling
	if brush_model != null:
		brush_model.transform = ora_inverse
	# now for collision!
	var scale_transform = Transform.IDENTITY.scaled(Vector3.ONE / config.scale_divisor)
	var collision_transform = ora_inverse * scale_transform
	build_collision(root, entity.brushes, tagged_convexes, collision_transform, config, collision)

# used here in build_entity, and in entity_converter_simple for packed scenes
static func determine_transform(target: Spatial, entity: MapIODataEntity, config: MapIOBuilderConfigLL, guess_allowed: bool):
	# handle angles first because origin does a lot of returning
	var angles = entity.angles()
	# Confirmed axis-to-axis mappings:
	# X -> -Z | Z -> X | Y -> Y
	var tfu = Transform.IDENTITY.rotated(Vector3.UP, deg2rad(angles.y)) # -- confirmed mapping --
	var tfb = Transform.IDENTITY.rotated(Vector3.BACK, deg2rad(-angles.x)) # -- confirmed mapping --
	var tfr = Transform.IDENTITY.rotated(Vector3.RIGHT, deg2rad(angles.z)) # -- confirmed mapping --
	# Application orders:
	# RUB: Failed on all combinations
	# URB: Failed on all combinations
	# UBR: THIS ONE!!!
	var transform = tfu * tfb * tfr
	transform.origin = _determine_origin(entity, config, guess_allowed)
	# IF YOU GET AN ERROR HERE:
	# Make absolutely sure sync_to_physics is enabled AS LATE AS POSSIBLE.
	# Godot's physics system really hates non-editor out-of-tree sync-to-physics kinematic bodies.
	target.transform = transform

static func _determine_origin(entity: MapIODataEntity, config: MapIOBuilderConfigLL, guess_allowed: bool):
	# origin time!
	if entity.keys.has("origin"):
		return entity.vector("origin") / config.scale_divisor
	# Pick the first brush as a fallback origin brush.
	# Then attempt to find a real origin brush.
	var origin_brush: MapIODataBrush = null
	if guess_allowed and (len(entity.brushes) > 0):
		origin_brush = entity.brushes[0]
	for v in entity.brushes:
		var brush: MapIODataBrush = v
		# Determine if brush contains any origin surfaces
		var has_origin = false
		for tn in brush.textures:
			if tn == config.texture_origin:
				origin_brush = brush
				has_origin = true
				break
		if has_origin:
			break
	# If the origin brush exists, use it.
	if origin_brush != null:
		var convex: MapIOMathsConvex = origin_brush.as_convex()
		return MapIOMathsBase.points_centre(convex.get_points()) / config.scale_divisor
	# No brushes and no explicit origin.
	return Vector3.ZERO

static func build_rendering(surfaces: Array, config: MapIOBuilderConfigLL, use_in_baked_light: bool, generate_lightmap: bool, portal_mode: int) -> Spatial:
	var portals = []
	var mb = MapIOBuilderMultimaterialMeshBuilder.new()
	for v in surfaces:
		var surface: MapIOSurfacingSurface = v
		if surface.material.areaportal:
			# Areaportal special stuff!
			# NOTES:
			# 1. This deliberately relies on the mesh-based portals conversion semantics.
			#    There is no other good way to do this.
			# 2. There's an implicit assumption here that the splitter won't create a wrongly-oriented areaportal.
			portals.push_back(build_portal(surface.winding, config.scale_divisor, portal_mode))
			pass
		else:
			surface.add_to_mesh_builder(mb)
	var brush_model = mb.commit_to_spatial(portal_mode, config.scale_divisor, use_in_baked_light, generate_lightmap, config.lightmap_texel_size)
	brush_model.name = "model"
	for portal in portals:
		brush_model.add_child(portal)
	return brush_model

static func build_portal(winding: PoolVector3Array, scale_divisor: float, pm: int) -> MeshInstance:
	var st = SurfaceTool.new()
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	st.add_triangle_fan(winding)
	var mi = MeshInstance.new()
	mi.name = "*" + str(mi.get_instance_id()) + "-portal"
	mi.mesh = st.commit()
	mi.portal_mode = pm
	mi.scale = Vector3.ONE / scale_divisor
	# workaround that these are still visible
	mi.visible = false
	return mi

static func build_collision(parent: Spatial, brushes: Array, tagged_convexes: Array, point_transform: Transform, config: MapIOBuilderConfigLL, collision_type: int):
	if collision_type == MapIOFGDEnums.COLLISION_TYPE.NONE:
		return
	assert(len(brushes) == len(tagged_convexes))
	if collision_type == MapIOFGDEnums.COLLISION_TYPE.CONCAVES:
		var tris = PoolVector3Array()
		var idx = 0
		while idx < len(brushes):
			var brush: MapIODataBrush = brushes[idx]
			var convex: MapIOMathsConvex = tagged_convexes[idx]
			idx += 1 # just do this here
			# no no-collide brushes here please
			if _brush_is_no_collide(brush, config):
				continue
			for w in convex.windings:
				var wt: PoolVector3Array = point_transform.xform(w)
				# convert winding to triangles!
				var pa = wt[0]
				var i = 2
				while i < len(w):
					tris.push_back(pa)
					tris.push_back(wt[i - 1])
					tris.push_back(wt[i])
					i += 1
		var shape = ConcavePolygonShape.new()
		shape.set_faces(tris)
		var cs = CollisionShape.new()
		cs.name = "brushes"
		cs.shape = shape
		parent.add_child(cs)
	else:
		var idx = 0
		while idx < len(brushes):
			var brush: MapIODataBrush = brushes[idx]
			var convex: MapIOMathsConvex = tagged_convexes[idx]
			idx += 1 # just do this here
			# no no-collide brushes here please
			if _brush_is_no_collide(brush, config):
				continue
			# holds the Shape
			var shape
			var translation = Vector3.ZERO
			if collision_type == MapIOFGDEnums.COLLISION_TYPE.BOXES:
				shape = BoxShape.new()
				var points = point_transform.xform(convex.get_points())
				var aabb: AABB = AABB(points[0], Vector3(0, 0, 0))
				for p in points:
					aabb = aabb.expand(p)
				shape.extents = aabb.size / 2
				translation = aabb.position + shape.extents
			else:
				shape = ConvexPolygonShape.new()
				shape.points = point_transform.xform(convex.get_points())
			var cs = CollisionShape.new()
			cs.name = "brush"
			cs.shape = shape
			cs.translation = translation
			parent.add_child(cs)

static func _brush_is_no_collide(brush: MapIODataBrush, config: MapIOBuilderConfigLL):
	for matname in brush.textures:
		var mat: MapIOBuilderMaterial = config.get_material(matname)
		if mat.no_collide:
			return true
	return false
