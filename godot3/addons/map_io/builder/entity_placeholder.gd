# Be aware that you don't need to (and maybe shouldn't) extend this script.
# Just add the exports below.
class_name MapIOBuilderEntityPlaceholder
extends Spatial

# for easy access
export var scale_divisor: float = 1.0
# MapIODataEntity
export var entity_resource: Resource
