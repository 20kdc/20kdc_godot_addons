# Simple entity converter you probably want to use in most cases.
# Provides a sort of "set of entity paths" system with the same basic semantics as how materials work.
class_name MapIOEntityConverterSimple
extends MapIOEntityConverter
# I'm not sure why this needs to be marked as a tool script when almost every other class has not.
tool

export var entity_roots = PoolStringArray(["res://baseq3/progs/", "res://addons/map_io/progs/"])
export var entity_filename_extensions = PoolStringArray([".tres", ".res", ".tscn", ".scn", ".gd"])
export var passive_location_markers = PoolStringArray(["info_player_start"])

func _mapio_convert_entity(entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	var cls = entity.classname()
	for v in passive_location_markers:
		if v == cls:
			context.add_location_unnamed(entity.vector("origin"))
			break
	for root in entity_roots:
		for ext in entity_filename_extensions:
			var path = root + cls + ext
			if ResourceLoader.exists(path):
				var inst = load(path)
				if inst is MapIOEntityConverter:
					# This is a simple enough way to give particular classnames complex functionality.
					# Think of this as similar to BSP-compiler-level entity shenanigans.
					return inst._mapio_convert_entity(entity, context)
				elif inst is Script:
					return mapio_convert_entity_default(inst, entity, context)
				elif inst is PackedScene:
					# Note that if using a PackedScene, brush models are essentially discarded.
					# (Origin brushes are still honoured though.)
					# Use a Script instead.
					if not entity.brushes.empty():
						push_warning("MapIO: Entity of classname \"" + cls + "\" was loaded with PackedScene \"" + path + "\" but has a brush model (discarded).")
					var inx = inst.instance()
					MapIOBuilderLL.attach_entity_data(inx, entity, context.config)
					MapIOBuilderLL.determine_transform(inx, entity, context.config, true)
					return inx
				else:
					push_warning("MapIO: Entity of classname \"" + cls + "\" was loaded with Resource \"" + path + "\" as an entity prototype that wasn't understood.")
	return mapio_convert_entity_default(MapIOBuilderEntityPlaceholder, entity, context)

# Default entity handling. This is what MapIO will do with any entity that it doesn't understand.
# This also contains the general handling for "*.gd" scripts.
static func mapio_convert_entity_default(script: Script, entity: MapIODataEntity, context: MapIOEntityConverterContext) -> Spatial:
	var has_brushes = not entity.brushes.empty()
	# Default "no script" defaults.
	var body_type = MapIOFGDEnums.BODY_TYPE.SPATIAL
	var rendering = MapIOFGDEnums.RENDERING_TYPE.DYNAMIC
	var collision = MapIOFGDEnums.COLLISION_TYPE.CONVEXES
	if has_brushes:
		body_type = MapIOFGDEnums.BODY_TYPE.KINEMATIC
	# Attempt to infer any obvious "the script wants X" cases that don't need manual specification.
	if script.get_instance_base_type() == "Area":
		body_type = MapIOFGDEnums.BODY_TYPE.AREA
	elif script.get_instance_base_type() == "StaticBody":
		body_type = MapIOFGDEnums.BODY_TYPE.STATIC
	elif script.get_instance_base_type() == "KinematicBody":
		body_type = MapIOFGDEnums.BODY_TYPE.KINEMATIC
	elif script.get_instance_base_type() == "RigidBody":
		body_type = MapIOFGDEnums.BODY_TYPE.RIGID
	# The script can define constants to override the guess.
	var constants = script.get_script_constant_map()
	if constants.has("MAPIO_BODY_TYPE"):
		body_type = constants["MAPIO_BODY_TYPE"]
	if constants.has("MAPIO_RENDERING_TYPE"):
		rendering = constants["MAPIO_RENDERING_TYPE"]
	if constants.has("MAPIO_COLLISION_TYPE"):
		collision = constants["MAPIO_COLLISION_TYPE"]
	# The keyvalues can define constants to override everything else.
	if entity.keys.has("rendering"):
		rendering = int(entity.keys["rendering"])
	if has_brushes:
		if entity.keys.has("body_type"):
			body_type = int(entity.keys["body_type"])
		if entity.keys.has("body_collision"):
			collision = int(entity.keys["body_collision"])
	# Do it.
	return MapIOBuilder.build_prop(script, entity, context.config, body_type, rendering, collision, true)
