# Contextual information for an entity conversion.
# The idea is that this is where information is placed to be sent to or received from other stages.
class_name MapIOEntityConverterContext
extends Reference
tool

var config: MapIOBuilderConfigLL
# it's worth noting this gets sorted by MapIONetworkingTraditional
var area_markers: Array

func _init():
	area_markers = []

func add_location(location: Vector3, name: String, explicit: bool, priority: int):
	var am = MapIOAreaMarker.new()
	am.location = location
	am.name = name
	am.explicit = explicit
	am.priority = priority
	area_markers.push_back(am)

func add_location_unnamed(location: Vector3):
	add_location(location, "Room", false, 0)
