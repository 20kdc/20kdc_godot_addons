# Profiling
class_name MapIOProfilerNull
extends Reference

func profile_open(_stage: String):
	pass
func profile_statistic(_statistic: String):
	pass
func profile_close():
	pass
func profile_complete(_ptr: String):
	pass
