class_name MapIOProfilerSignal
extends MapIOProfilerNull

signal message(text)

func profile_open(stage: String):
	emit_signal("message", stage)
func profile_statistic(statistic: String):
	emit_signal("message", statistic)
