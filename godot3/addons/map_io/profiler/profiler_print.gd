class_name MapIOProfilerPrint
extends MapIOProfilerNull

var profiler_stack: Array
var stages: Array

func _init():
	profiler_stack = []

func profile_open(stage: String):
	var element = [stage]
	profiler_stack.push_back(element)
	stages.push_back(element) # [0]
	element.push_back(OS.get_ticks_usec()) # [1]
func profile_statistic(statistic: String):
	print(statistic)
func profile_close():
	var st = OS.get_ticks_usec()
	var element: Array = profiler_stack.pop_back()
	element.push_back(st) # [2]
	print(element[0] + " took " + str(st - element[1]) + " microseconds")
func profile_complete(ptr: String):
	# convert to SpeedScope format as GDScript JSON
	var frames = []
	var events = []
	# create events and frames
	for v in stages:
		var stage = v[0]
		var stage_start = v[1]
		var stage_end = v[2]
		var frame = len(frames)
		frames.push_back({"name": stage})
		events.push_back({"type": "O", "frame": frame, "at": stage_start})
		events.push_back({"type": "C", "frame": frame, "at": stage_end})
	# sort events
	events.sort_custom(self, "_event_compare")
	# determine start/end times
	var startValue = 0
	var endValue = 0
	if len(events) > 0:
		startValue = events[0]["at"]
		endValue = events[len(events) - 1]["at"]
	# generate JSON framing
	var spc = {
		"version": "0.0.1",
		"$schema": "https://www.speedscope.app/file-format-schema.json",
		"shared": {
			"frames": frames
		},
		"profiles": [
			{
				"type": "evented",
				"name": ptr + " @ " + str(startValue),
				"unit": "microseconds",
				"startValue": startValue,
				"endValue": endValue,
				"events": events
			}
		]
	}
	# return
	var f = File.new()
	f.open(ptr, File.WRITE)
	f.store_line(to_json(spc))
	f.close()

func _event_compare(a: Dictionary, b: Dictionary):
	return a["at"] < b["at"]
