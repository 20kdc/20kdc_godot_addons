extends Resource
class_name MidasEvent

# Time in seconds. This is worked out during loading.
export var time: float = 0.0
export var time_original: int = 0
# This used to be a PoolByteArray, but since those are in scarce supply, that was changed.
export var event: Array = []
