extends Resource
class_name MidasFile, "../icons/midas_file.png"

const MTHD_FORMAT_ONE_TRACK = 0
const MTHD_FORMAT_MULTI_TRACK = 1
const MTHD_FORMAT_MULTI_SONG = 2

var mthd_format: int = MTHD_FORMAT_ONE_TRACK
# Array of arrays.
# Each element of the outer array is a track array.
# Each element of a track array is a KAuMidasEvent.
var tracks: Array = []

func read(f : File):
	var old_es = f.endian_swap
	f.endian_swap = true
	var rc: MidasRiffChunk = MidasRiffChunk.new()
	rc.read(f)
	# Should check for MThd & length, really
	mthd_format = rc.get_ub16()
	var track_count: int = rc.get_ub16()
	var mthd_division = rc.get_sb16()
	for i in range(0, track_count):
		var trk: Array = []
		rc.read(f)
		# parse MIDI events
		_parse_midi_track(trk, rc)
		tracks.append(trk)
	if mthd_format == MTHD_FORMAT_MULTI_SONG:
		# multi-song format, per-song timing calculations
		for trk in tracks:
			_calc_timings([trk], mthd_division)
	else:
		# single-song format, timing calculations across whole song
		_calc_timings(tracks, mthd_division)
	f.endian_swap = old_es

func _calc_timings(tracks: Array, division: int):
	var ptrs := []
	var tir = range(len(tracks))
	for trk in tracks:
		ptrs.push_back(0)
	var last_time := 0
	var last_time_s := 0.0
	# tempo
	var s_quarternotemicroseconds := 500000
	var factor := _calc_timing_factor(division, s_quarternotemicroseconds)
	# continue
	while true:
		var best_idx = -1
		var best_idx_time = 0
		# find earliest event
		for tidx in tir:
			if ptrs[tidx] < len(tracks[tidx]):
				var lev: MidasEvent = tracks[tidx][ptrs[tidx]]
				var lt = lev.time_original
				if best_idx == -1 or (best_idx_time > lt):
					best_idx = tidx
					best_idx_time = lt
		if best_idx == -1:
			break
		var ev: MidasEvent = tracks[best_idx][ptrs[best_idx]]
		var delta = best_idx_time - last_time
		last_time = best_idx_time
		last_time_s += delta * factor
		ev.time = last_time_s
		ptrs[best_idx] += 1
		# calculation
		if ev.event[0] == 0xFF:
			if ev.event[1] == 0x51:
				# Tempo control. Contains microseconds per beat.
				# But a beat is not a deltaTime, so use an additional division.
				s_quarternotemicroseconds = (ev.event[3] << 16) | (ev.event[4] << 8) | ev.event[5]
				factor = _calc_timing_factor(division, s_quarternotemicroseconds)
			elif ev.event[1] == 0x54:
				print("midas warning: FF54 showed up")
			elif ev.event[1] == 0x58:
				print("midas warning: FF58 showed up")

func _calc_timing_factor(division: int, s_quarternotemicroseconds: int) -> float:
	if division < 0:
		var tca := division >> 8
		var tcb := division & 0xFF
		var fps := 30
		if tca == -24:
			fps = 24
		elif tca == -25:
			fps = 25
		return 1.0 / float(tcb * fps)
	# division = ticks per quarter-note.
	# quarter-notes are expressed in microseconds.
	var qns = float(s_quarternotemicroseconds) / 1000000.0
	return qns / float(division)

func _parse_midi_track(trk: Array, rc: MidasRiffChunk):
	# In delta-units, whatever that is
	var now: int = 0
	# this dummy value is used if a running status is immediately used
	#  without being established, but also serves to ensure that for the
	#  real-time events, running statuses aren't falsely triggered
	var cache: int = 255
	while rc.ptr < rc.data.size():
		var delta = rc.get_vli()
		now += delta
		var event_start_ptr = rc.ptr
		var new_cache = _skip_event(cache, rc)

		var dat: MidasEvent = MidasEvent.new()
		dat.time_original = now
		# Work around some semantics nonsense with PoolByteArray.insert
		#  by holding this in a temp var
		# Otherwise running event prefixes fail to show up for no reason
		var event_array: PoolByteArray = rc.data.subarray(event_start_ptr, rc.ptr - 1)
		if new_cache != 0:
			# standard event
			cache = new_cache
		else:
			# running event
			#print("RUNNING STATUS ACKNOWLEDGED: " + str(cache))
			event_array.insert(0, cache)
		dat.event = Array(event_array)
		trk.append(dat)

# Skips an event.
# Is passed the current running-status.
# Returns 0 to indicate that we just used running-status.
# Otherwise, returns the new value of running-status.
func _skip_event(cached_cmd : int, rc : MidasRiffChunk) -> int:
	var true_cmd : int = rc.get_u8()
	var cmd : int = true_cmd
	if cmd < 0x80:
		# running status is being used!
		# go back one, load in the old command,
		# then run forward
		rc.ptr -= 1
		cmd = cached_cmd
		# This zero is used to signal inserting a fake command byte
		true_cmd = 0
		#print("RUNNING STATUS!!! actual command: " + str(cmd))

	if cmd < 0xC0:
		rc.ptr += 2
		return true_cmd
	if cmd < 0xE0:
		rc.ptr += 1
		return true_cmd
	if cmd < 0xF0:
		rc.ptr += 2
		return true_cmd
	if cmd == 0xF0:
		while rc.get_u8() != 0xF7:
			continue
		return true_cmd
	if cmd == 0xF1:
		rc.ptr += 1
		return true_cmd
	if cmd == 0xF2:
		rc.ptr += 2
		return true_cmd
	if cmd == 0xF3:
		rc.ptr += 1
		return true_cmd
	# F4/F5 undefined, F6 defined with no bytes, F7 End Of Exclusive so...
	if cmd == 0xFF:
		# NOTE: This is Reset according to MIDI stuff,
		#  but it's something entirely different for MIDI files
		rc.ptr += 1
		# The evaluation order makes a direct += not work for this.
		var vli = rc.get_vli()
		rc.ptr += vli
		# This is real-time
		return cached_cmd
	if cmd >= 0xF8:
		# These are *also* real-time
		return cached_cmd
	# print("got odd command " + str(true_cmd))
	return true_cmd
