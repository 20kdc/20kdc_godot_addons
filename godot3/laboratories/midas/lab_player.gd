extends Button

export var synth_path: NodePath
var fd = FileDialog.new()
var current_player: MidasPlayer

func _ready():
	fd.access = FileDialog.ACCESS_FILESYSTEM
	fd.mode = FileDialog.MODE_OPEN_FILE
	$"..".call_deferred("add_child", fd)
	fd.connect("confirmed", self, "_file_confirmed")

func _pressed():
	fd.set_size(Vector2(512, 512))
	fd.popup_centered()
	fd.show_modal(true)

func _file_confirmed():
	if current_player != null:
		current_player.free()
		current_player = null
	print("Loading " + fd.current_path + " now!")
	var fil = load(fd.current_path)
	# --
	var events = 0
	var max_time = 0.0
	for track in fil.tracks:
		for event in track:
			events += 1
			max_time = max(max_time, event.time)
	print(str(events) + " total events, max time " + str(max_time))
	# --
	current_player = MidasPlayer.new()
	current_player.source = fil
	current_player.connect("midi_event", get_node(synth_path), "midi_event")
	add_child(current_player)
