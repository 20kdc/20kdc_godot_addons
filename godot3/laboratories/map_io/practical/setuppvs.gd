extends Node

export var enabled: bool = true

func _ready():
	if enabled:
		get_parent().rooms_convert()
