# Generates reference ZIPReader info

extends TextEdit

func _ready():
	var text := ""
	var zr := ZIPReader.new()
	text += str(zr.open("../common-test-data/test.zip")) + "\n"
	for v in zr.get_files():
		var data := zr.read_file(v)
		var hc := HashingContext.new()
		hc.start(HashingContext.HASH_MD5)
		if len(data) > 0:
			hc.update(data)
		var hx := hc.finish().hex_encode()
		text += "F: " + v + ": " + str(len(data)) + ": " + hx + "\n"
	text += "CICheck: " + str(len(zr.read_file("mouse/loud/here.txt", false))) + "\n"
	zr.read_file("ThisFileDoesNotExist")
	text += str(zr.close()) + "\n"
	self.text = text
