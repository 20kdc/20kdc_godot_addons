extends Node2D

var strips: Array

var udp = PacketPeerUDP.new()

var points: PoolVector2Array

func _ready():
	strips = [
		[null, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16]], # face outline
		[null, [17, 18, 19, 20, 21]], # right eyebrow (left in image)
		[null, [22, 23, 24, 25, 26]], # left eyebrow (right in image)
		[null, [27, 28, 29, 30]], # nose main line
		[null, [31, 32, 33, 34, 35]], # nose under line
		[null, [36, 37, 38, 39, 40, 41, 36]], # right eye (left in image)
		[null, [42, 43, 44, 45, 46, 47, 42]], # left eye (right in image)
		[null, [48, 49, 50, 51, 52, 62, 53, 54, 55, 56, 57, 58, 48]], # mouth outer
		[null, [59, 60, 61, 63, 64, 65, 59]], # mouth inner
		[null, [39, 66]], # right eye
		[null, [42, 67]], # left eye
	]
	for v in strips:
		var l2d = Line2D.new()
		l2d.width = 1
		v[0] = l2d
		add_child(l2d)
	udp.listen(11573)
	points.resize(SkymoonOpenSeeFaceFace.LANDMARK_COUNT)

func _process(delta):
	while udp.get_available_packet_count() > 0:
		var packet = udp.get_packet()
		var face = SkymoonOpenSeeFaceFace.new()
		face.from_buffer(packet)
		var offset = face.image_size / -2
		for i in range(SkymoonOpenSeeFaceFace.LANDMARK_COUNT):
			points[i] = face.landmarks_pos[i] + offset
		for i in range(SkymoonOpenSeeFaceFace.FEATURE_COUNT):
			find_node("feature_" + str(i)).value = (face.features[i] + 1) * 50
		find_node("MeshInstance").rotation_degrees = face.converted_euler()
	update_points()

func update_points():
	for v in strips:
		var l2d: Line2D = v[0]
		var newpoints = PoolVector2Array()
		for vx in v[1]:
			newpoints.push_back(points[vx])
		l2d.points = newpoints

