extends Button

func _pressed():
	var res = AImgIOAPNGImporter.load_from_file(($"../LineEdit").text)
	if res[0] != null:
		push_error(res[0])
	else:
		var i = 0
		for v in res[1]:
			var fv: AImgIOFrame = v
			fv.content.save_png("a_scratchpad/a/" + str(i) + ".png")
			i += 1
