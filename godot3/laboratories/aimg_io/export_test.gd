extends Button

func _pressed():
	var fidx = 0
	var frames = []
	while true:
		var f = AImgIOFrame.new()
		var img = Image.new()
		if img.load("a_scratchpad/a/" + str(fidx) + ".png") == OK:
			img.convert(Image.FORMAT_RGBA8)
			f.content = img
			f.duration = 0.1
			frames.push_back(f)
			fidx += 1
		else:
			break
	var exporter = AImgIOAPNGExporter.new()
	var res = exporter.export_animation(frames, 10, self, "_ignored", [])
	var o = File.new()
	o.open("a_scratchpad/a.png", File.WRITE)
	o.store_buffer(res)
	o.close()

func _ignored():
	pass
