# 20kdc's Godot Addons

A set of pure-GDScript (for maximum portability) Godot addons.

## Structure

This repository is split into Godot 3 and Godot 4 sections.

The Godot 4 section is presently empty.

Figuring out how to keep the two sections in sync is a matter of ongoing research.

*When in doubt, the Godot 3 version will be preferred due to hardware compatibility issues with Godot 4.*

## License

The license of this repository has been changed from CC0 to Unlicense.

This is for better compatibility with the Fedora changes.

*This is expected to not remove any existing rights you have as a user of this repository, nor to take away from rights you would otherwise have.*

(And if it does, then it's almost certainly not something I can handle without making things worse.)

* https://lwn.net/ml/fedora-legal/CAC1cPGw1xScGAXo-0NRs92zFB7ptRxTt=oCYi0BxfZDfAgUtYQ@mail.gmail.com/
* https://fedoraproject.org/wiki/Licensing/Unlicense

As such:

```
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org>
```

Copies of this text are stored as:

* `COPYING.txt` here
* `COPYING.txt` in addons where copies of those addons have been forwarded to other projects by me specifically.

## Versioning policy

You're expected not to try and treat this repository as an "upstream" that you have to stay in constant sync with.

I actively expect and encourage you to copy the addon into your repository if you're using it.

Ideally, a versioning policy would be in place, but GDScript is not a language which makes defining an "external API" an easy task.

With that firmly in mind, use your own judgement and test when doing changes.

