@tool
class_name TBWVertex
extends Marker3D

@export
var uv: Vector2

signal transform_changed()

func _notification(what: int) -> void:
	if what == NOTIFICATION_TRANSFORM_CHANGED:
		emit_signal("transform_changed")
