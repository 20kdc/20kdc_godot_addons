@tool
class_name TBWFace
extends TBWMesh

@export
var a: TBWVertex: set = _set_a
@export
var b: TBWVertex: set = _set_b
@export
var c: TBWVertex: set = _set_c
@export
var d: TBWVertex: set = _set_d

func tbw_write_to_surfacetool(into: SurfaceTool, transform: Transform3D):
	var invert_me = global_transform.inverse() * transform
	super.tbw_write_to_surfacetool(into, transform)
	if (a == null) or (b == null) or (c == null):
		return
	into.set_uv(a.uv)
	into.add_vertex(a.global_position * invert_me)
	into.set_uv(b.uv)
	into.add_vertex(b.global_position * invert_me)
	into.set_uv(c.uv)
	into.add_vertex(c.global_position * invert_me)
	if d != null:
		into.set_uv(a.uv)
		into.add_vertex(a.global_position * invert_me)
		into.set_uv(c.uv)
		into.add_vertex(c.global_position * invert_me)
		into.set_uv(d.uv)
		into.add_vertex(d.global_position * invert_me)

func _set_a(va: TBWVertex):
	_tri_disconnect()
	a = va
	_tri_reconnect()

func _set_b(vb: TBWVertex):
	_tri_disconnect()
	b = vb
	_tri_reconnect()

func _set_c(vc: TBWVertex):
	_tri_disconnect()
	c = vc
	_tri_reconnect()

func _set_d(vd: TBWVertex):
	_tri_disconnect()
	d = vd
	_tri_reconnect()

func _tri_disconnect():
	if a != null:
		a.disconnect("transform_changed", tbw_queue_rebake)
	if b != null:
		b.disconnect("transform_changed", tbw_queue_rebake)
	if c != null:
		c.disconnect("transform_changed", tbw_queue_rebake)
	if d != null:
		d.disconnect("transform_changed", tbw_queue_rebake)

func _tri_reconnect():
	if a != null:
		a.connect("transform_changed", tbw_queue_rebake)
	if b != null:
		b.connect("transform_changed", tbw_queue_rebake)
	if c != null:
		c.connect("transform_changed", tbw_queue_rebake)
	if d != null:
		d.connect("transform_changed", tbw_queue_rebake)
	tbw_queue_rebake()
