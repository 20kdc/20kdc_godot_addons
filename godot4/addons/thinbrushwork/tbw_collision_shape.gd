@tool
class_name TBWCollisionShape
extends CollisionShape3D

@export
var base: TBWMesh: set = _set_base

func _set_base(b: TBWMesh):
	if base != null:
		base.disconnect("baked", _update_shape)
	base = b
	if base != null:
		base.connect("baked", _update_shape)
	_update_shape(base.mesh)

func _update_shape(mesh: Mesh):
	if mesh != null:
		shape = mesh.create_trimesh_shape()
	else:
		shape = null
