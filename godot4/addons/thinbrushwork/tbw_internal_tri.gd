@tool
extends RefCounted
class_name TBWInternalTri

@export
var ac: Vector3
@export
var bc: Vector3
@export
var cc: Vector3
@export
var auv: Vector2
@export
var buv: Vector2
@export
var cuv: Vector2
@export
var asg: int
@export
var bsg: int
@export
var csg: int
@export
var material: Material
