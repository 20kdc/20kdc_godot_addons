@tool
class_name TBWMesh
extends MeshInstance3D

## Fires when this mesh is baked to a non-null mesh.
signal baked(mesh: ArrayMesh)

enum MeshBakeMode {
	ALWAYS = 0,
	EDITOR = 1,
	NEVER = 2
}

## This can either be 'Always', 'Editor', or 'Never'.
## It controls if the Mesh of the MeshInstance3D will be automatically updated on changes.
@export_enum("Always", "Editor", "Never")
var mesh_bake_mode := 0 : set = _tbw_set_bake

## Controlling TBWMesh.
## If set, then we're subservient and our mesh instance needs to go away.
var _controller: TBWMesh = null

## Rebake queue variable. This is to stop cascades from transform changes.
var _rebake_queued: bool = false

## Writes the contents of this mesh to a SurfaceTool.
## The vertices are in local space and are then transformed by the transform.
## Importantly, this always works regardless of bake mode.
func tbw_write_to_surfacetool(into: SurfaceTool, transform: Transform3D):
	_write_children_to_surfacetool(into, self)

## Writes child TBWMeshes into SurfaceTool.
func _write_children_to_surfacetool(into: SurfaceTool, node: Node):
	for child in node.get_children():
		if child is TBWMesh:
			# accounts for top_level etc.
			var rel_transform: Transform3D = child.global_transform * global_transform.inverse()
			child.tbw_write_to_surfacetool(into, rel_transform)
		else:
			_write_children_to_surfacetool(into, child)

func _notification(what: int) -> void:
	if what == NOTIFICATION_PARENTED:
		# controller is "most root" TBWMesh.
		var check = get_parent()
		_controller = null
		while check != null:
			if check is TBWMesh:
				_controller = check
				break
			check = check.get_parent()
		tbw_queue_rebake()
	elif what == NOTIFICATION_TRANSFORM_CHANGED:
		# transform changes affect us
		tbw_queue_rebake()

func _tbw_set_bake(new_val: int):
	_rebake_queued = false
	mesh_bake_mode = new_val
	tbw_queue_rebake()

## Force a (soon) rebake of whichever node is responsible for this node.
## See tbw_rebake for actual logic.
func tbw_queue_rebake():
	if !_tbw_bake_mode_check():
		_rebake_queued = false
		return
	if _controller != null:
		# forward queuing instantly
		mesh = null
		_rebake_queued = false
		_controller.tbw_queue_rebake()
		return
	if not _rebake_queued:
		_rebake_queued = true
		call_deferred("_tbw_handle_rebake_queue")

func _tbw_handle_rebake_queue():
	if _rebake_queued:
		tbw_rebake()

func _tbw_bake_mode_check() -> bool:
	if mesh_bake_mode == MeshBakeMode.NEVER:
		return false
	if (mesh_bake_mode == MeshBakeMode.EDITOR) and not Engine.is_editor_hint():
		return false
	return true

## Force a rebake of whichever node is responsible for this node.
## Importantly, this respects the bake mode.
func tbw_rebake():
	_rebake_queued = false
	if !_tbw_bake_mode_check():
		return
	# alright, we are updating mesh
	if _controller != null:
		mesh = null
		_controller.tbw_rebake()
		return
	# *actually* update mesh
	var surfacetool := SurfaceTool.new()
	surfacetool.begin(Mesh.PRIMITIVE_TRIANGLES)
	tbw_write_to_surfacetool(surfacetool, Transform3D.IDENTITY)
	surfacetool.index()
	surfacetool.generate_normals()
	surfacetool.generate_tangents()
	# commit & send signal
	var new_mesh: ArrayMesh = surfacetool.commit()
	mesh = new_mesh
	emit_signal("baked", new_mesh)
