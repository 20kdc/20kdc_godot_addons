extends TextureRect

var dt = 0.0
var frames: Array[AImgIOFrame] = []
var fidx = 0

func _ready():
	load_from("laboratories/aimg_io/import_demo.png")

func load_from(fn: String):
	var res = AImgIOAPNGImporter.load_from_file(fn)
	if res[0] == null:
		frames = res[1] as Array[AImgIOFrame]

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if len(frames) == 0:
		return
	if fidx >= len(frames):
		fidx = 0
	dt += delta
	var cframe: AImgIOFrame = frames[fidx]
	if dt >= cframe.duration:
		dt -= cframe.duration
		fidx += 1
	# yes this does this every _process, oh well
	var tex = ImageTexture.create_from_image(cframe.content)
	texture = tex
