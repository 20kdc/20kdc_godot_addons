extends Button

var accdt := 0.0
var frames : Array[AImgIOFrame] = []
var recording := false

func _ready():
	_update_text()

func _update_text():
	if recording:
		text = "Stop Recording"
	else:
		text = "Start Recording"

func _pressed():
	if not recording:
		recording = true
	else:
		recording = false
		var exporter := AImgIOAPNGExporter.new()
		var res := exporter.export_animation(frames, 10, self, "_progress_report", [])
		var file := FileAccess.open("test.png", FileAccess.WRITE)
		file.store_buffer(res)
		file.close()
		frames = []
		%TextureRect.load_from("test.png")
	_update_text()

func _progress_report():
	pass

func _process(delta):
	accdt += delta
	while accdt > 0.1:
		accdt -= 0.1
		if recording:
			var tex: ViewportTexture = %SubViewport.get_texture()
			var f := AImgIOFrame.new()
			f.content = tex.get_image()
			f.duration = 0.1
			frames.append(f)
