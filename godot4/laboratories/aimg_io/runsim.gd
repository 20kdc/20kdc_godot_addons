extends Button

@export var env: Node = null

func _pressed():
	if env != null:
		env.queue_free()
	env = preload("res://laboratories/rube/rube.tscn").instantiate()
	%SubViewport.add_child(env)
